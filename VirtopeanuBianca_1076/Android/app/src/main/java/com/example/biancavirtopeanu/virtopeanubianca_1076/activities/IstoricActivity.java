package com.example.biancavirtopeanu.virtopeanubianca_1076.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;

import java.util.ArrayList;
import java.util.List;

import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.AbstractActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Constants;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Rezultat;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.RezultatAdapter;

public class IstoricActivity extends AbstractActivity {

    private ListView list;
    private Rezultat r;
    private TextView name;
    static List<Rezultat>listaRezultate = new ArrayList<>();
    private SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_istoric);
        setTitle(R.string.app_main_istoric);
        list = (ListView)findViewById(R.id.istoric_list);
        Intent intent = getIntent();
        r=(Rezultat)intent.getSerializableExtra("REZULTAT");
        if(r!=null){
            listaRezultate.add(r);
        }
        RezultatAdapter adapterRez =new RezultatAdapter(getApplicationContext(),R.layout.istoric_list,listaRezultate,getLayoutInflater());
        list.setAdapter(adapterRez);
        preferences = getSharedPreferences(Constants.
                MAIN_SHARED_PREFERENCES_NAME, MODE_PRIVATE);
        name=(TextView)findViewById(R.id.istoric_name);
        //preluare feedback din fisier
        String nameFromFile = preferences.getString(Constants.
                MAIN_FEEDBACK_NAME, null);

        if (nameFromFile!=null) {
            name.setText("Buna " +nameFromFile +",");
        }
    }



}
