package com.example.biancavirtopeanu.virtopeanubianca_1076.utils;

/**
 * Created by Bianca Virtopeanu on 11/11/2017.
 */

public class Intrebare {
    private Long id;
    private String text;
    private  Raspuns rasp1;
    private Raspuns rasp2;
    private Raspuns rasp3;
    private Raspuns rasp4;
    private Raspuns rasp5;

    public Intrebare(Long id, String text, Raspuns rasp1, Raspuns rasp2, Raspuns rasp3, Raspuns rasp4, Raspuns rasp5) {
        this.id = id;
        this.text = text;
        this.rasp1 = rasp1;
        this.rasp2 = rasp2;
        this.rasp3 = rasp3;
        this.rasp4 = rasp4;
        this.rasp5 = rasp5;
    }

    public Intrebare(String text, Raspuns rasp1, Raspuns rasp2, Raspuns rasp3, Raspuns rasp4, Raspuns rasp5) {
        this.text = text;
        this.rasp1 = rasp1;
        this.rasp2 = rasp2;
        this.rasp3 = rasp3;
        this.rasp4 = rasp4;
        this.rasp5 = rasp5;
    }

    public Intrebare() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Raspuns getRasp1() {
        return rasp1;
    }

    public void setRasp1(Raspuns rasp1) {
        this.rasp1 = rasp1;
    }

    public Raspuns getRasp2() {
        return rasp2;
    }

    public void setRasp2(Raspuns rasp2) {
        this.rasp2 = rasp2;
    }

    public Raspuns getRasp3() {
        return rasp3;
    }

    public void setRasp3(Raspuns rasp3) {
        this.rasp3 = rasp3;
    }

    public Raspuns getRasp4() {
        return rasp4;
    }

    public void setRasp4(Raspuns rasp4) {
        this.rasp4 = rasp4;
    }

    public Raspuns getRasp5() {
        return rasp5;
    }

    public void setRasp5(Raspuns rasp5) {
        this.rasp5 = rasp5;
    }
}
