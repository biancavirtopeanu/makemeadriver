package com.example.biancavirtopeanu.virtopeanubianca_1076.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;

import java.util.HashMap;
import java.util.Map;

public class CapitolActivity extends AppCompatActivity {

    private Map<Integer,String> capitol = new HashMap<>();
    Intent intent;
    private String titlu;
    private int nrCapitol;
    private String text;
    private TextView titluTv;
    private TextView textTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_capitol);
        intent=getIntent();
        nrCapitol=intent.getIntExtra("ID_CAPITOL",0);
        titlu = intent.getStringExtra("NUME_CAPITOL");
        titluTv=(TextView)findViewById(R.id.capitol_titlu);
        textTv=(TextView)findViewById(R.id.capitol_text);
        titluTv.setText(titlu);

        switch(nrCapitol){
            case 1:{
                textTv.setText(R.string.capitol_text);break;
            }
            case 2:{
                textTv.setText(R.string.capitol2_text);break;
            }
            case 3:{
                textTv.setText(R.string.capitol3_text);break;
            }
            case 4:{
                textTv.setText(R.string.capitol4_text);break;
            }
            case 5:{
                textTv.setText(R.string.capitol5_text);break;
            }
            case 6:{
                textTv.setText(R.string.capitol6_text);break;
            }
            case 7:{
                textTv.setText(R.string.capitol7_text);break;
            }
            case 8:{
                textTv.setText(R.string.capitol8_text);break;
            }
            case 9:{
                textTv.setText(R.string.capitol9_text);break;
            }
            case 10:{
                textTv.setText(R.string.capitol10_text);break;
            }
        }

    }


}
