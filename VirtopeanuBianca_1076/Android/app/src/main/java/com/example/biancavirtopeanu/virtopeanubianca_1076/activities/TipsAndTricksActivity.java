package com.example.biancavirtopeanu.virtopeanubianca_1076.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.AbstractActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Constants;

public class TipsAndTricksActivity extends AbstractActivity {

    private Button inainte;
    private Button inapoi;
    private TextView text;
    int index=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips_and_tricks);
        setTitle(R.string.app_main_tips);
        inainte=(Button)findViewById(R.id.tips_inainte);
        inapoi=(Button)findViewById(R.id.tips_inapoi);
        text=(TextView)findViewById(R.id.tips_text);

        text.setText(Constants.tips[index]);
        index++;
        inainte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(index< Constants.tips.length-1){
                    text.setText(Constants.tips[index]);
                    index++;
                }else{
                    index=0;
                    text.setText(Constants.tips[index]);
                }
            }
        });

        inapoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(index>0){
                    index--;
                    text.setText(Constants.tips[index]);
                }
            }
        });

    }


}
