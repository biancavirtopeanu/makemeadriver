package com.example.biancavirtopeanu.virtopeanubianca_1076.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Bianca Virtopeanu on 12/10/2017.
 */

public class DatabaseController extends SQLiteOpenHelper implements  DbConstants {
    private static DatabaseController controller = null;

    private DatabaseController(Context context) {
        super(context,
                DATABASE_NAME,
                null,
                DATABASE_VERSION);
    }

    public static DatabaseController getInstance(Context context) {

        synchronized (DatabaseController.class) {
            if (controller == null) {
                controller = new
                        DatabaseController(context);
            }
        }

        return controller;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_RASPUNS);
            db.execSQL(CREATE_TABLE_INTREBARE);
            db.execSQL(CREATE_TABLE_FEEDBACK);
//            db.execSQL(ALTER_TABLE_INTREBARE);
//            db.execSQL(ALTER_TABLE_INTREBARE2);
//            db.execSQL(ALTER_TABLE_INTREBARE3);
//            db.execSQL(ALTER_TABLE_INTREBARE4);
//            db.execSQL(ALTER_TABLE_INTREBARE5);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db,
                          int oldVersion,
                          int newVersion) {
        try {
            db.execSQL(DROP_TABLE_INTREBARE);
            db.execSQL(DROP_TABLE_RASPUNS);
            db.execSQL(DROP_TABLE_FEEDBACK);
            onCreate(db);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
