package com.example.biancavirtopeanu.virtopeanubianca_1076.firebase;

import android.util.Log;

import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Indicator;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

/**
 * Created by Bianca Virtopeanu on 1/3/2018.
 */

public class FirebaseController implements FirebaseConstant {
    private DatabaseReference database;
    private static FirebaseController firebaseController;
    private FirebaseDatabase fcontroller;
    private boolean responseInsert;

    private FirebaseController() {
        fcontroller = FirebaseDatabase.getInstance();
    }

    public static FirebaseController getInstance() {

        synchronized (FirebaseController.class) {
            if (firebaseController == null) {
                firebaseController = new FirebaseController();
            }
        }

        return firebaseController;
    }

    public boolean addPlayer(Indicator indicator) {

        responseInsert = false;
        if (indicator == null)
            return responseInsert;

        database = fcontroller.getReference("indicatoare");
        //genereaza un id pentru inregistrarea ta.
        if (indicator.getGlobalId() == null || indicator.getGlobalId().trim().isEmpty()) {
            indicator.setGlobalId(database.push().getKey());
        }
        database.child(indicator.getGlobalId()).setValue(indicator);
        addChangeEventListenerForEachIndicator(indicator);

        return responseInsert;
    }

    private void addChangeEventListenerForEachIndicator(Indicator indicator) {
        //acest eveniment se declanseaza pentru orice modificare adusa fiecarui jucator din firebase
        database.child(indicator.getId().toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Indicator temp = dataSnapshot.getValue(Indicator.class);
                if (temp != null) {
                    responseInsert = true;
                    Log.i("FireBaseController", "Updated indicator: " + temp.toString());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("FirebaseController", "Insert is not working");
            }
        });
    }

    public boolean addAllIndicatoare(List<Indicator> indicators) {

        if (indicators == null || indicators.size() == 0)
            return false;

        for (Indicator indicator : indicators) {
            addPlayer(indicator);
        }

        return true;
    }

    public void findAllIndicator(ValueEventListener eventListener) {
        if (eventListener != null)
            database = fcontroller.getReference(TABLE_NAME_INDICATOARE);
        database.addValueEventListener(eventListener);
    }

}