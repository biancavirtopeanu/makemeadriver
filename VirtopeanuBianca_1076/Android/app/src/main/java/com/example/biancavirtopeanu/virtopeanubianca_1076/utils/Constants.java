package com.example.biancavirtopeanu.virtopeanubianca_1076.utils;

import java.text.SimpleDateFormat;

/**
 * Created by Bianca Virtopeanu on 11/18/2017.
 */

    public interface Constants {

        Integer ADD_REZULTAT_REQUEST_CODE = 100;
        String ADD_DATA_REZULTAT_DATE_FORMAT = "dd-MM-yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ADD_DATA_REZULTAT_DATE_FORMAT);

    //JSON TAGS
    String JSON_CATEGORIE="categorie";
    String JSON_TIP="tip";
    String JSON_DIFICULTATE="dificultate";
    String JSON_NR_INTREBARI="nrIntrebari";
    String JSON_INTREBARI="intrebari";
    String JSON_TEXT_INTREBARE="text";
    String JSON_RASP1="raspuns1";
    String JSON_RASP2="raspuns2";
    String JSON_RASP3="raspuns3";
    String JSON_RASP4="raspuns4";
    String JSON_RASP5="raspuns5";
    String JSON_TEXT_RASPUNS="textR";
    String JSON_VALID="valid";
    String JSON_EXTRA_INFO="extraInfo";
    String JSON_NR_CHESTIOANR="nrChestionar";
    String JSON_ITEM="item";
    String JSON_EXTRA_INFO_RASP="extraInfoR";

    //SHARED PREGERENCES
    String MAIN_SHARED_PREFERENCES_NAME = "mainPref";
    String MAIN_FEEDBACK_NAME = "name";

    //TRANSFER PARAMETRII
    String rezultat = "REZULTAT";
    String punctaj="PUNCTAJ";
    String numeCapitol="NUME_CAPITOL";
    String idCapitol="ID_CAPITOL";
    String timp="TIMP";

    //INDICATOARE TEXT
    String i1Ref="\tCedeaza trecerea\n" +
            "Se amplaseaza pe drumul public fara prioritate, la intersectia acestuia cu un drum public prioritar. La intalnirea acestui indicator, conducatorul auto este obligat sa reduca viteza si sa se asigure ca pe drumul prioritar nu circula autovehicule, si abia apoi poate sa patrunda in intersectie. Daca pe drumul prioritar circula alte autovehicule, conducatorul auto este obligat sa opreasca pentru a le acorda prioritate.";
    String i2Ref="\tOprire\n" +
            "Indicatorul Oprire este instalat pe drumul public fara prioritate, la intersectia acestuia cu un drum public prioritar, cand vizibilitatea este redusa. Conducatorul de vehicul care intalneste acest indicator este obligat sa opreasca in locul cu vizibilitate maxima, fara a depasi coltul intersectiei, si sa acorde prioritate tuturor vehiculelor care circula pe drumul prioritar.";
    String i3Ref="\tDrum cu prioritate\n" +
            "Indicatorul acesta se instaleaza la inceputul drumului cu prioritate sau inaintea intersectiilor cu un drum fara prioritate. Are rolul de a anunta conducatorul auto ca are prioritate in intersectiile in care va patrunde. Drumurile care intersecteaza drumul cu prioritate vor avea instalate unul dintre indicatoarele Oprire sau Cedeaza trecerea. Cand este amplasat inaintea unei intersectii, acest indicator poate fi insotit si de un panou aditional care va preciza directia drumului cu prioritate.";
    String i4Ref="\tSfarsitul drumului cu prioritate\n" +
            "Indicatorul Sfarsitul drumului cu prioritate se amplaseaza la 50-200 m de locul unde inceteaza prioritatea indicatorului Drum cu prioritate, avand rolul de a informa conducatorul auto ca in urmatoarea intersectie in care va patrunde, toate drumurile vor fi de aceeasi categorie si se va aplica regula prioritatii de dreapta.";
    String i5Ref="\tPrioritate fata de circulatia din sens invers\n" +
            "Montat pe sectoarele de drum ingustat unde nu au loc sa circule doua vehicule unul pe langa celalalt indicatorul din imagine este precedat de indicatorul Drum ingustat. La intalnirea acestui indicator, conducatorul autovehiculului are prioritatea fata de toate autovehiculele care circula din sens opus.";
    String i6Ref="\tCurba la stanga\n" +
            "Este amplasat la cel mult 200 m de o curba la stanga. Canducatorul trebuie sa circule cu viteza redusa in curbe, iar daca vizibilitatea este redusa, toate manevrele (depasirea, oprirea, stationarea, mersul inapoi, intoarcerea) sunt interzise.";
    String i7Ref="\tCurba la dreapta\n" +
            "Este amplasat la cel mult 200 m de o curba la dreapta. Canducatorul trebuie sa circule cu viteza redusa in curbe, iar daca vizibilitatea este redusa, toate manevrele (depasirea, oprirea, stationarea, mersul inapoi, intoarcerea) sunt interzise.";
    String i8Ref="Curba dubla sau o succesiune de mai multe curbe, prima la stanga\n" +
            "Indicatorul din imagine se instaleaza atunci cand urmeaza o succesiune de curbe, daca distanta dintre acestea e mai mica de 250 m. Se amplaseaza la 100-200 m inaintea primei curbe. Aceste indicatoare pot fi insotite si de panouri aditionale, pe care este specificata distanta pana la terminarea sectorului de drum periculos, daca lungimea acestuia depaseste 1000 m. Canducatorul trebuie sa circule cu viteza redusa in curbe, iar daca vizibilitatea este redusa, toate manevrele (depasirea, oprirea, stationarea, mersul inapoi, intoarcerea) sunt interzise.";
    String i9Ref="\tCurba dubla sau o succesiune de mai multe curbe, prima la dreapta\n" +
            "Indicatorul din imagine se instaleaza atunci cand urmeaza o succesiune de curbe, daca distanta dintre acestea e mai mica de 250 m. Se amplaseaza la 100-200 m inaintea primei curbe. Aceste indicatoare pot fi insotite si de panouri aditionale, pe care este specificata distanta pana la terminarea sectorului de drum periculos, daca lungimea acestuia depaseste 1000 m. Canducatorul trebuie sa circule cu viteza redusa in curbe, iar daca vizibilitatea este redusa, toate manevrele (depasirea, oprirea, stationarea, mersul inapoi, intoarcerea) sunt interzise.";
    String PYRAMID_DATA="DATA";

    //TIPS & TRICKS
    String [] tips  = new String[]{
        "Da-te la o parte atunci cand mergi prea incet!Nu este o rusine ca nu poti tine pasul cu ceilalti – poate nu ai experienta, poate ai un vehicul mai lent, poate pur si simplu ti-e teama sa mergi mai repede ori doar esti foarte prudent – dar nu-i obliga nici pe ceilalti sa se tina in spatele tau cu zecile de kilometri. Trage pur si simplu pe dreapta in primul spatiu in care poti face aceasta manevra in siguranta si lasa-i pe ceilalti sa treaca pe langa tine, dupa care reintra pe sosea."
            ,"Nu accelera atunci cand altii te-ar putea depasi!"
            ,"Atunci cand observi ca banda din dreapta sau din stanga ta se termina ori ca este ocupata de un obstacol – gen autovehicul oprit, lucrare, surpare etc – lasa-i si pe soferii de pe banda respectiva sa intre pe banda ta, pentru a-si putea continua deplasarea. Aplica regula fermoarului – o masina de pe banda ta, o masina de pe banda cealalta. Ai putea fi oricand in situatia celuilalt sofer si ai vrea sa fii lasat sa intri pe banda libera."
            ,"Nu da flash-uri din spate celor care depasesc pe autostrada!"
            ,"Nu depasi coloanele in asteptare! Aseaza-te la rand!"
            ,"Privește mai departe de mașina din față. Dacă te uiți doar la mașina din fața ta îți reduci dramatic timpul de reacție. Învață-te să privești înainte (vreo 10 secunde de drum, cum ar veni) și să observi din timp variațiile din trafic. Poate că cineva pune o frână bruscă.",
            "Dacă banda din dreapta ta este liberă, ești pe banda greșită. Asta este pentru autostradă și drumurile expres cu două benzi și nici nu trebuie explicată cine știe ce, sper."
    };
}




