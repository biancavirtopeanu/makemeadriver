package com.example.biancavirtopeanu.virtopeanubianca_1076.network;

/**
 * Created by Bianca Virtopeanu on 12/3/2017.
 */

public class Item {
    private int nrChestionar;
    private String extraInfo;
    private CategorieInfo categorie;

    public Item(int nrChestionar, String extraInfo, CategorieInfo categorie) {
        this.nrChestionar = nrChestionar;
        this.extraInfo = extraInfo;
        this.categorie = categorie;
    }

    public Item() {
    }

    public int getNrChestionar() {
        return nrChestionar;
    }

    public void setNrChestionar(int nrChestionar) {
        this.nrChestionar = nrChestionar;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public CategorieInfo getCategorie() {
        return categorie;
    }

    public void setCategorie(CategorieInfo categorie) {
        this.categorie = categorie;
    }

    @Override
    public String toString() {
        return "Item{" +
                "nrChestionar=" + nrChestionar +
                ", extraInfo='" + extraInfo + '\'' +
                ", categorie=" + categorie +
                '}';
    }
}
