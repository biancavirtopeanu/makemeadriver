package com.example.biancavirtopeanu.virtopeanubianca_1076.firebase;

import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

/**
 * Created by Bianca Virtopeanu on 1/3/2018.
 */

public class CloudStorageUploads {

    FirebaseStorage storage;
    // Create a storage reference from our app
    StorageReference storageRef;

    // Create a reference to "mountains.jpg"
    public StorageReference i1Ref;
    public StorageReference i2Ref;
    public StorageReference i3Ref;
    public StorageReference i4Ref;
    public StorageReference i5Ref;
    public StorageReference i6Ref;
    public StorageReference i7Ref;
    public StorageReference i8Ref;
    public StorageReference i9Ref;

    public  ArrayList<StorageReference> indicatoareRef = new ArrayList<>();
    public void createRef(){
        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        StorageReference storageRef = storage.getReference();
        // i1Ref = storageRef.child("i1.gif");
       i2Ref = storageRef.child("indicatoare/i2.gif");
        i3Ref = storageRef.child("indicatoare/i3.gif");
         i4Ref = storageRef.child("indicatoare/i4.gif");
        i5Ref = storageRef.child("indicatoare/i5.gif");
        i6Ref = storageRef.child("indicatoare/i6.gif");
        i7Ref = storageRef.child("indicatoare/i7.gif");
        i8Ref = storageRef.child("indicatoare/i8.gif");
       i9Ref = storageRef.child("indicatoare/i9.gif");
    }
    public  ArrayList<StorageReference>getIndicatoareRef(){
        indicatoareRef.add(i1Ref);
        indicatoareRef.add(i2Ref);
        indicatoareRef.add(i3Ref);
        indicatoareRef.add(i4Ref);
        indicatoareRef.add(i5Ref);
        indicatoareRef.add(i6Ref);
        indicatoareRef.add(i7Ref);
        indicatoareRef.add(i8Ref);
        indicatoareRef.add(i9Ref);
        return  indicatoareRef;
    }
}
