package com.example.biancavirtopeanu.virtopeanubianca_1076.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;

import java.util.ArrayList;
import java.util.Date;

import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Constants;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Rezultat;

public class RezultatChestionarActivity extends AppCompatActivity {

   private TextView punctajTv;
    private Button veziIstoric;
    private Rezultat rezultat;
    private Button piramida;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rezultat_chestionar);
        punctajTv=(TextView)findViewById(R.id.rezultat_punctaj);
        veziIstoric=(Button)findViewById(R.id.rezultat_vezi_istoric_btn);
        piramida=(Button)findViewById(R.id.piramida_rezultat);
        rezultat=new Rezultat();
        rezultat.setData(new Date());
        final Intent intent = getIntent();
        rezultat.setPunctaj(intent.getIntExtra("PUNCTAJ",0));
        rezultat.setTimp(intent.getIntExtra("TIMP",0));
        punctajTv.setText("Ai obtinut " + rezultat.getPunctaj()+ "puncte.");
        veziIstoric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),IstoricActivity.class);
                intent.putExtra("REZULTAT",rezultat);
                startActivity(intent);
            }
        });

        piramida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               ArrayList<Integer>data= intent.getIntegerArrayListExtra(Constants.PYRAMID_DATA);
               Intent i = new Intent(getApplicationContext(),PyramidActivity.class);
               i.putIntegerArrayListExtra(Constants.PYRAMID_DATA,data);
               startActivity(i);
            }
        });


    }
}
