package com.example.biancavirtopeanu.virtopeanubianca_1076.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Constants;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Feedback;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Intrebare;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Raspuns;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Bianca Virtopeanu on 12/10/2017.
 */

public class DatabaseRepository implements DbConstants {
    SQLiteDatabase database;
    DatabaseController controller;

    public DatabaseRepository(Context context) {
        controller = DatabaseController
                .getInstance(context);
    }

    public void open() {
        try {
            database = controller.
                    getWritableDatabase();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void deleteAndOpen(){
        try{
            database=controller.getWritableDatabase();
            database.execSQL("DELETE FROM "+TABLE_NAME_INTREBARE);
            database.execSQL("DELETE FROM "+TABLE_NAME_RASPUNS);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            database.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private ContentValues createContentValuesFromRaspuns(Raspuns r) {

        ContentValues contentValues = new ContentValues();

        contentValues.put(COLUMN_RASPUNS_TEXT, r.getText() != null ? r.getText() : "");
        contentValues.put(COLUMN_RASPUNS_VALID, r.isValid());

        return contentValues;
    }
    public Long insertRaspuns(Raspuns r){
        if(r == null){
            return -1L;
        }
        return database.insert(TABLE_NAME_RASPUNS,null, createContentValuesFromRaspuns(r));
    }

    public  int updateRaspuns(Raspuns r){
        if(r==null || r.getId()==null ){
            return -1;
        }
        return database.update(TABLE_NAME_RASPUNS, createContentValuesFromRaspuns(r), COLUMN_RASPUNS_ID + "=?", new String[]{r.getId().toString()});
    }

    public List<Raspuns> selectAllRaspuns() {

        List<Raspuns> results = new ArrayList<>();

        Cursor cursor = database.query(TABLE_NAME_RASPUNS, null, null, null, null, null, null);


        while (cursor.moveToNext() != false) {
            Long id = cursor.getLong(cursor.getColumnIndex(COLUMN_RASPUNS_ID));

            Date date = null;

            String text = cursor.getString(cursor.getColumnIndex(COLUMN_RASPUNS_TEXT));
            Integer valid = cursor.getInt(cursor.getColumnIndex(COLUMN_RASPUNS_VALID));

            results.add(new Raspuns(id, text, valid==1));
        }

        cursor.close();

        return results;
    }

    public int deleteRaspuns(Raspuns r) {
        try {
            if (r == null || r.getId() == null) {
                return -1;
            }

            return database.delete(TABLE_NAME_RASPUNS, COLUMN_RASPUNS_ID + "=" + r.getId().toString(), null);

        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            return -2;
        }
    }

    private ContentValues createContentValuesFromIntrebare(Intrebare i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_INTREBARE_TEXT, i.getText() != null ? i.getText() : "");
        contentValues.put(COLUMN_INTREBARE_RASP1,i.getRasp1().getId());
        contentValues.put(COLUMN_INTREBARE_RASP2,i.getRasp2().getId());
        contentValues.put(COLUMN_INTREBARE_RASP3,i.getRasp3().getId());
        contentValues.put(COLUMN_INTREBARE_RASP4,i.getRasp4().getId());
        contentValues.put(COLUMN_INTREBARE_RASP5,i.getRasp5().getId());
        return contentValues;
    }

    public Long insertIntrebare(Intrebare i){
        if(i == null){
            return -1L;
        }
        return database.insert(TABLE_NAME_INTREBARE,null, createContentValuesFromIntrebare(i));
    }

    public int updateIntrebare(Intrebare i){
        if(i==null || i.getId()==null){
            return -1;
        }
        return database.update(TABLE_NAME_INTREBARE,createContentValuesFromIntrebare(i), COLUMN_INTREBARE_ID + "=?", new String[]{i.getId().toString()});
    }

    public Raspuns getRaspunsById(int id){
        Cursor cursor = database.query(TABLE_NAME_RASPUNS, new String[] { COLUMN_RASPUNS_ID,
                        COLUMN_RASPUNS_TEXT, COLUMN_RASPUNS_VALID }, COLUMN_RASPUNS_ID + "="+id,null
                , null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Raspuns r= new Raspuns();
        try {
            r.setText(cursor.getString(cursor.getColumnIndex(COLUMN_RASPUNS_TEXT)));
            r.setValid(1 == cursor.getInt(cursor.getColumnIndex(COLUMN_RASPUNS_VALID)));
            cursor.close();
            return r;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }


    }
    public List<Intrebare> findAllIntrebari() {

        List<Intrebare> results = new ArrayList<>();

        Cursor cursor = database.query(TABLE_NAME_INTREBARE, null, null, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        while (cursor.moveToNext() != false) {
            Long id = cursor.getLong(cursor.getColumnIndex(COLUMN_INTREBARE_ID));
            String text = cursor.getString(cursor.getColumnIndex(COLUMN_INTREBARE_TEXT));
            Raspuns rasp1 = getRaspunsById(cursor.getInt(cursor.getColumnIndex(COLUMN_INTREBARE_RASP1)));
            Raspuns rasp2 = getRaspunsById(cursor.getInt(cursor.getColumnIndex(COLUMN_INTREBARE_RASP2)));
            Raspuns rasp3 = getRaspunsById(cursor.getInt(cursor.getColumnIndex(COLUMN_INTREBARE_RASP3)));
            Raspuns rasp4 = getRaspunsById(cursor.getInt(cursor.getColumnIndex(COLUMN_INTREBARE_RASP4)));
            Raspuns rasp5 = getRaspunsById(cursor.getInt(cursor.getColumnIndex(COLUMN_INTREBARE_RASP5)));
            results.add(new Intrebare(id,text, rasp1, rasp2, rasp3, rasp4, rasp5));
        }
        cursor.close();
        return results;
    }


    public Long insertFeedback(Feedback f){
        if(f == null){
            return -1L;
        }
        return database.insert(FEEDBACK_TABLE_NAME,null, createContentValuesFromFeedback(f));
    }

    private ContentValues createContentValuesFromFeedback(Feedback f) {

        ContentValues contentValues = new ContentValues();

        contentValues.put(FEEDBACK_COLUMN_EMAIL, f.getEmail() != null ? f.getEmail() : "");
        contentValues.put(FEEDBACK_COLUMN_NUME, f.getNume() != null ? f.getNume() : "");
        contentValues.put(FEEDBACK_COLUMN_PRENUME, f.getPrenume() != null ? f.getPrenume() : "");
        contentValues.put(FEEDBACK_COLUMN_FEEDBACK_TEXT, f.getFeedbackText() != null ? f.getFeedbackText() : "");
        contentValues.put(FEEDBACK_COLUMN_FEEDBACK_EMAIL_CHECKED, f.isEmailChecked()  ? 1 : 0);
        return contentValues;
    }

    public List<Feedback> getAllFeedback(){
        List<Feedback> results = new ArrayList<>();

        Cursor cursor = database.query(FEEDBACK_TABLE_NAME, null, null, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        while (cursor.moveToNext() != false) {
            Long id = cursor.getLong(cursor.getColumnIndex(FEEDBACK_COLUMN_ID));
            String feedbackText = cursor.getString(cursor.getColumnIndex(FEEDBACK_COLUMN_FEEDBACK_TEXT));
            String nume = cursor.getString(cursor.getColumnIndex(FEEDBACK_COLUMN_NUME));
            String prenume = cursor.getString(cursor.getColumnIndex(FEEDBACK_COLUMN_PRENUME));
            String email = cursor.getString(cursor.getColumnIndex(FEEDBACK_COLUMN_EMAIL));
            int emailChecked=cursor.getInt(cursor.getColumnIndex(FEEDBACK_COLUMN_FEEDBACK_EMAIL_CHECKED));
            Feedback f = new Feedback(id,nume,prenume,email,feedbackText);
            f.setEmailChecked(emailChecked==1);
            results.add(f);
        }
        cursor.close();
        return results;
    }
}
