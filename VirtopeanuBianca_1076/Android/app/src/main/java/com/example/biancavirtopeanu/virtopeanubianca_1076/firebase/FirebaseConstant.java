package com.example.biancavirtopeanu.virtopeanubianca_1076.firebase;

/**
 * Created by Bianca Virtopeanu on 1/3/2018.
 */

public interface FirebaseConstant {
     String TABLE_NAME_INDICATOARE="indicatoare";
     String TABLE_NAME_TIPS="tips";
}
