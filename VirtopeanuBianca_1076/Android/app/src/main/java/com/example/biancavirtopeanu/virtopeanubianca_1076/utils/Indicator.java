package com.example.biancavirtopeanu.virtopeanubianca_1076.utils;

/**
 * Created by Bianca Virtopeanu on 1/3/2018.
 */

public class Indicator {
    private String globalId;
    private Long id;
    private String indicatorName;
    private String indicatorText;

    public Indicator(String globalId, Long id, String indicatorName, String indicatorText) {
        this.globalId = globalId;
        this.id = id;
        this.indicatorName = indicatorName;
        this.indicatorText = indicatorText;
    }

    public Indicator(String indicatorName, String indicatorText) {
        this.indicatorName = indicatorName;
        this.indicatorText = indicatorText;
    }

    public Indicator() {
    }

    public String getGlobalId() {
        return globalId;
    }

    public void setGlobalId(String globalId) {
        this.globalId = globalId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIndicatorName() {
        return indicatorName;
    }

    public void setIndicatorName(String indicatorName) {
        this.indicatorName = indicatorName;
    }

    public String getIndicatorText() {
        return indicatorText;
    }

    public void setIndicatorText(String indicatorText) {
        this.indicatorText = indicatorText;
    }

    @Override
    public String toString() {
        return "Indicator{" +
                "globalId='" + globalId + '\'' +
                ", id=" + id +
                ", indicatorName='" + indicatorName + '\'' +
                ", indicatorText='" + indicatorText + '\'' +
                '}';
    }
}
