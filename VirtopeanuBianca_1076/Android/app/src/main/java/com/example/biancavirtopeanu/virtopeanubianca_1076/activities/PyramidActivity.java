package com.example.biancavirtopeanu.virtopeanubianca_1076.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;
import com.example.biancavirtopeanu.virtopeanubianca_1076.graphics.PyramidChart;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class PyramidActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pyramid);

        Intent intent = getIntent();

        if (intent != null && intent.hasExtra(Constants.PYRAMID_DATA)) {

            ArrayList<Integer> data = intent.getIntegerArrayListExtra(Constants.PYRAMID_DATA);
            PyramidChart view = new PyramidChart(getApplicationContext());

            view.setData(intent.getIntegerArrayListExtra(Constants.PYRAMID_DATA));
            setContentView(view);
        } else {

            setContentView(R.layout.activity_pyramid);
        }
    }
}
