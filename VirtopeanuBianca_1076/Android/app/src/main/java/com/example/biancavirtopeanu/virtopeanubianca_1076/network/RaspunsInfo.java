package com.example.biancavirtopeanu.virtopeanubianca_1076.network;

import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Raspuns;

/**
 * Created by Bianca Virtopeanu on 12/3/2017.
 */

public class RaspunsInfo extends Raspuns {
    private String text;
    private boolean valid;
    private String extraInfo;

    public RaspunsInfo(Long id, String text, boolean valid, String text1, boolean valid1, String extraInfo) {
        super(id, text, valid);
        this.text = text1;
        this.valid = valid1;
        this.extraInfo = extraInfo;

    }

    public RaspunsInfo() {
    }

    public RaspunsInfo(String text, boolean valid, String text1, boolean valid1, String extraInfo) {
        super(text, valid);
        this.text = text1;
        this.valid = valid1;
        this.extraInfo = extraInfo;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }
}
