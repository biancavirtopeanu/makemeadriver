package com.example.biancavirtopeanu.virtopeanubianca_1076.utils;

/**
 * Created by Bianca Virtopeanu on 12/23/2017.
 */

public class Feedback {
    private Long id;
    private String nume;
    private String prenume;
    private String email;
    private String feedbackText;
    private boolean emailChecked;

    public boolean isEmailChecked() {
        return emailChecked;
    }

    public void setEmailChecked(boolean emailChecked) {
        this.emailChecked = emailChecked;
    }

    public Feedback(Long id, String nume, String prenume, String email) {
        this.id = id;
        this.nume = nume;
        this.prenume = prenume;
        this.email = email;
    }

    public Feedback(Long id, String nume, String prenume, String email, String feedbackText) {
        this.id = id;
        this.nume = nume;
        this.prenume = prenume;
        this.email = email;
        this.feedbackText = feedbackText;
    }

    public Feedback(String nume, String prenume, String email, String feedbackText) {
        this.nume = nume;
        this.prenume = prenume;
        this.email = email;
        this.feedbackText = feedbackText;
    }

    public String getFeedbackText() {
        return feedbackText;
    }

    public void setFeedbackText(String feedbackText) {
        this.feedbackText = feedbackText;
    }

    public Feedback(String nume, String prenume, String email) {
        this.nume = nume;
        this.prenume = prenume;
        this.email = email;
    }

    public Feedback() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
