package com.example.biancavirtopeanu.virtopeanubianca_1076.utils;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.biancavirtopeanu.virtopeanubianca_1076.activities.ChestionareActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.activities.FeedbackActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.activities.IndicatoareActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.activities.IstoricActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.activities.LegislatieRutieraActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.activities.MainActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.activities.ModulInvatareActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.R;
import com.example.biancavirtopeanu.virtopeanubianca_1076.activities.TipsAndTricksActivity;

/**
 * Created by Bianca Virtopeanu on 11/4/2017.
 */

public class AbstractActivity extends AppCompatActivity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
//        Intent intent= null;
//        switch(item.getItemId()){
//
//            case R.id.app_menu_feedback:{
//                intent=new Intent(getApplicationContext(),FeedbackActivity.class);
//                break;
//            }
//
//            case R.id.app_menu_legislatie:{
//                intent=new Intent(getApplicationContext(),LegislatieRutieraActivity.class);
//                break;
//            }
//            case R.id.app_menu_chestionare:{
//                intent = new Intent(getApplicationContext(), ChestionareActivity.class);
//                break;
//            }
//            case R.id.app_menu_modulInvatare:{
//                intent=new Intent(getApplicationContext(),ModulInvatareActivity.class);
//                break;
//            }
//
//            case R.id.app_menu_istoric:{
//                intent=new Intent(getApplicationContext(),IstoricActivity.class);
//                break;
//            }
//            case R.id.app_menu_tips:{
//                intent = new Intent(getApplicationContext(), TipsAndTricksActivity.class);
//                break;
//            }
//            case R.id.app_menu_indicatoare:{
//                intent = new Intent(getApplicationContext(), IndicatoareActivity.class);
//                break;
//            }
//            default:{
//                intent=new Intent(getApplicationContext(),MainActivity.class);
//                break;
//            }
//
//        }

//        startActivity(intent);
//        Toast.makeText(getApplicationContext(),item.getTitle(),Toast.LENGTH_SHORT).show();
//        return true;
  }
}
