package com.example.biancavirtopeanu.virtopeanubianca_1076.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;

import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.AbstractActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Constants;

public class LegislatieRutieraActivity extends AbstractActivity implements Constants

{

    Spinner spinner;
    private static final String[] capitole = {"Captolul 1: Dispozitii generale",
                                "Capitolul 2: Vehiculele",
                                "Capitolul 3: Conducatorii de vehicule",
                                "Capitolul 4: Semnalizarea rutiera",
                                "Capitolul 5: Reguli de circulatie",
                                "Capitolul 6: Infractiuni si pedepse",
                                "Capitolul 7: Raspunderea contraventionala",
                                "Capitolul 8: Cai de atac impotriva procesului-verbal de constatare a contraventiei",
                                "Capitolul 9: Atributii ale unor ministere si ale altor autoritati ale administratiei publice",
                                "Capitolul 10:Dispozitii finale"};
    private ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legislatie_rutiera);
        spinner=(Spinner)findViewById(R.id.legislatie_rutiera_spinner);
        ArrayAdapter<String> adatpter=new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,capitole);
        spinner.setAdapter(adatpter);
        setTitle(R.string.app_main_legislatie);
        image=(ImageView)findViewById(R.id.legislatieRutiera_image);
        final Intent intent = new Intent(getApplicationContext(),CapitolActivity.class);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),"Acum selecteaza lumina verde!",Toast.LENGTH_SHORT);
                String selectedItem = parent.getItemAtPosition(position).toString();
                switch (selectedItem){
                    case "Captolul 1: Dispozitii generale":{
                        intent.putExtra(idCapitol,1);
                        intent.putExtra(numeCapitol,capitole[0]);

                        break;
                    }
                    case "Capitolul 2: Vehiculele":{
                        intent.putExtra(idCapitol,2);
                        intent.putExtra(numeCapitol,capitole[1]);
                        break;
                    }
                    case "Capitolul 3: Conducatorii de vehicule":{
                        intent.putExtra(idCapitol,3);
                        intent.putExtra(numeCapitol,capitole[2]);
                        break;
                    }
                    case "Capitolul 4: Semnalizarea rutiera":{
                        intent.putExtra(idCapitol,4);
                        intent.putExtra(numeCapitol,capitole[3]);
                        break;
                    }
                    case "Capitolul 5: Reguli de circulatie":{
                        intent.putExtra(idCapitol,5);
                        intent.putExtra(numeCapitol,capitole[4]);
                        break;
                    }
                    case "Capitolul 6: Infractiuni si pedepse":{
                        intent.putExtra(idCapitol,6);
                        intent.putExtra(numeCapitol,capitole[5]);
                        break;
                    }
                    case "Capitolul 7: Raspunderea contraventionala":{
                        intent.putExtra(idCapitol,7);
                        intent.putExtra(numeCapitol,capitole[6]);
                        break;
                    }
                    case "Capitolul 8: Cai de atac impotriva procesului-verbal de constatare a contraventiei":{
                        intent.putExtra(idCapitol,8);
                        intent.putExtra(numeCapitol,capitole[7]);
                        break;
                    }
                    case "Capitolul 9: Atributii ale unor ministere si ale altor autoritati ale administratiei publice":{
                        intent.putExtra(idCapitol,9);
                        intent.putExtra(numeCapitol,capitole[8]);
                        break;
                    }
                    case "Capitolul 10:Dispozitii finale":{
                        intent.putExtra(idCapitol,10);
                        intent.putExtra(numeCapitol,capitole[9]);
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        image.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(intent);
            }
        });
    }





}
