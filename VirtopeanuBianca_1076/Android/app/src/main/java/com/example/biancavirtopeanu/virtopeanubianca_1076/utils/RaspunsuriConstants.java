package com.example.biancavirtopeanu.virtopeanubianca_1076.utils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Bianca Virtopeanu on 12/23/2017.
 */

public interface RaspunsuriConstants {
    public String [] raspunsuriTexts = new String[]{
                "sa reduca viteza, fara a depasi linia continua pentru oprire;",
                "sa opreasca autovehiculul in locul unde vizibilitatea este maxima, fara a depasi indicatorul de prioritate in cruce;",
                "sa reduca viteza autovehiculului si sa traverseze cu atentie, asigurandu-se ca din partea stanga sau din partea dreapta nu se apropie un vehicul feroviar.",
                "niciuna de mai sus",
                "toate de mai sus",
                "creati conditii pentru a fi depasit, apropiindu-va cat mai mult de partea dreapta a drumului public"
                ,"il atentionati atat cu claxonul, cat si cu bratul stang, pentru a renunta la aceasta manevra;"
                ,"va deplasati spre stanga, din timp, pentru a nu-i permite efectuarea acestei manevre."
                ,"niciunul de mai sus"
                ,"toate de mai sus"
                ,"intr-o intersectie nedirijata, atunci cand patrundeti pe un drum national, venind de pe un drum judetean;"
                ,"la intalnirea indicatorului ``Drum cu prioritate``"
                ,"la intalnirea pietonilor aflati pe sensul de mers opus."
                ,"niciunul de mai sus"
                ,"toate de mai sus"
                ,"da, daca autovehiculul a blocat circulatia;"
                ,"da, daca accidentul nu s-a produs din vina sa;"
                ," nu, intrucat fapta constituie infractiune"
                ,"incertitudine"
                ,"toate de mai sus"
                ,"anual, la orice service auto;"
                ,"periodic, conform legii;"
                ,"la cel mult doi ani, la o statie de inspectie tehnica autorizata."
                ,"niciunul de mai sus"
                ,"toate de mai sus"
                ,"da;"
                ,"nu",
                "legea nu prevede",
                "niciunul de mai sus",
                "incertitudine",
                "cand autoturismul are efectuata inspectia tehnica",
                "cand autoturismul a fost asigurat de raspundere civila in caz de pagube materiale produse tertilor prin accidente de circulatie, conform legii;"
                ,"conform legii,niciodata"
                ,"doar atunci cand se primeste instiintare"
                ,"cand autoturismul nu are montata una din placutele cu numarul de inmatriculare."
            ,"in zona de actiune a indicatorului \"Oprirea interzisa\", pana la prima intersectie;"
            ,"pe partea carosabila a drumurilor nationale."
            ,"pe trotuar"
            ,"in general peste tot"
            ,"pe banda 1"
            ,"sa menti viteza constanta"
            ,"sa conduci doar pe drumurile pe care le cunosti"
            ,"da dai prioritate de stanga"
            ,"legal nu ai nicio obligatie",
            " sa adaptati viteza in functie de conditiile de drum, astfel incat sa puteti efectua orice manevra in conditii de siguranta;"
            ,"pe banda 1"
            ,"in curbe, cand vizibilitatea este redusa sub 50 m"
            ,"in zona de actiune a indicatorului \"Stationarea interzisa\";"
            ,"e permis mereu"
            ,"legea nu specifica"
            ,"legea nu specifica"
            ,"raspuns incert"
            ,"pe partea carosabila a oricarei benzi pe oricare tip de drum"
            ,"la mai putin de 50 m de cel mai apropiat colt al intersectiei"
            ," la o distanta mai mica de 25 m inainte si dupa indicatorul statiei pentru mijloacele de transport public de persoane."
            ,"in apropierea varfurilor de rampa, cand vizibilitatea este sub 50 m;"
            ,"inainte de intersectii"
            ,"este permisa tot timpul"
            ,"pe autostrada"
            ,"legea nu specifica"
            ,"110 km/h pe autostrazi, 90 km/h pe drumurile expres sau nationale europene si 80 km/h pe celelalte categorii de drumuri;"
            ,"90 km/h pe autostrazi, 80 km/h pe drumurile expres sau nationale europene si 70 km/h pe celelalte categorii de drumuri;"
            ,"130 km/h pe autostrazi si 90 km/h pe celelalte categorii de drumuri."
            ,"toate de mai sus"
            ,"niciuna de mai sus"
            ,"folositi frana de urgenta si trageti brusc de volan"
            ,"franati usor"
            ,"bruscati comenzile directiei"
            ,"intrati in ea"
            ,"catapultati-va din masina"
            ,"strada pe care urmeaza sa se intre este semnalizata cu indicatorul \"Drum fara iesire\";"
            ,"conducatorul de vehicul este incadrat pe banda din dreapta;"
            ,"drumul pe care urmeaza sa se intre este semnalizat cu indicatorul \"Circulatia interzisa in ambele sensuri\"."
            ,"niciuna de mai sus"
            ,"toate de mai sus"
            ,"infundarea filtrului de aer"
            ,"pornirea"
            ,"oprirea brusca"
            ,"toate de mai sus"
            ,"niciuna de mai sus"
            ,"cand nu ii merg luminile de ceata"
            ,"cand soferul are permisul de doar 1 an"
            ,"cand polueaza fonic in timpul mersului sau staionarii la cote admise de lege"
            ,"toate variantele sunt corecte"
            ,"cand nu este dotat cu placute de inmatriculare"
            ,"pune sau menţine în funcţiune faza de drum, pentru a observa mai bine drumul pe direcţia de deplasare;"
            ,"aprinde alternativ faza de drum şi de întâlnire, pentru a-l atenţiona pe celălalt conducător că circulă incorect;"
            ,"menţine în funcţiune luminile de întâlnire, reduce viteza de deplasare şi opreşte dacă este cazul."
            ,"toate de mai sus"
            ,"niciuna de mai sus"
            ,"neincetinirea la trecerea de pieton atunci cand nu sunt pietoni ce doresc sa traverseze"
            ,"nerespectarea semnalelor poliţiştilor la trecerea coloanelor oficiale;"
            ,"neoprirea la semnalul persoanelor autorizate din zona de lucrări la drumul public;"
            ,"circularea cu luminile de ceata pornite atunci cand nu e ceata"
            ,"circularea cu farurile aprinse in localitate"
            ,"pe drumurile secundare;"
            ,"pe drumurile tertiare;"
            ,"pe drumurile cu cel puţin două benzi pe sens."
            ,"niciodata nu este permis"
            ,"pe drumurile cu sens unic, dacă rămâne liberă cel puţin o bandă de circulaţie"
            ,"nicio obligatie"
            ,"sa isi inchida masina"
            ,"să nu oprească prea aproape de următorul autovehicul."
            ,"să acţioneze frâna de ajutor, să oprească funcţionarea motorului şi să cupleze o treaptă inferioară de viteză;"
            ,"să-i semnalizeze prezenţa pe timp de noapte cu o sursă de lumină;"
            ,"legea nu prevede"
            ,"nu sunt legate de domeniul auto"
            ,"dublează marcajele convenţionale în zonele periculoase;"
            ,"avertizează conducătorii de autovehicule că depăşesc zona de carosabil marcată şi destinată sensului de deplasare;"
            ,"avertizează conducătorii de autovehicule că vor pătrunde într-o zonă supravegheată cu aparatură radar."
            ,"daca nu vine trenul,da"
            ,"nu se poate stii"
            ,"da, dacă aceasta nu a ajuns încă la linia de tren"
            ,"da, întrucât căruţa este un vehicul lent;"
            ,"nu",
            "sa reduca viteza, fara a depasi linia continua pentru oprire;",
            "sa opreasca autovehiculul in locul unde vizibilitatea este maxima, fara a depasi indicatorul de prioritate in cruce;",
            "sa reduca viteza autovehiculului si sa traverseze cu atentie, asigurandu-se ca din partea stanga sau din partea dreapta nu se apropie un vehicul feroviar.",
            "niciuna de mai sus",
            "toate de mai sus",
            "creati conditii pentru a fi depasit, apropiindu-va cat mai mult de partea dreapta a drumului public"
            ,"il atentionati atat cu claxonul, cat si cu bratul stang, pentru a renunta la aceasta manevra;"
            ,"va deplasati spre stanga, din timp, pentru a nu-i permite efectuarea acestei manevre."
            ,"niciunul de mai sus"
            ,"toate de mai sus"
            ,"intr-o intersectie nedirijata, atunci cand patrundeti pe un drum national, venind de pe un drum judetean;"
            ,"la intalnirea indicatorului ``Drum cu prioritate``"
            ,"la intalnirea pietonilor aflati pe sensul de mers opus."
            ,"niciunul de mai sus"
            ,"toate de mai sus"



    };
}
