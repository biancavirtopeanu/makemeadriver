package com.example.biancavirtopeanu.virtopeanubianca_1076.utils;

/**
 * Created by Bianca Virtopeanu on 11/11/2017.
 */

public class Raspuns {
    private  Long id;
    private String text;
    private boolean valid;

    public Raspuns(Long id, String text, boolean valid) {
        this.id = id;
        this.text = text;
        this.valid = valid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Raspuns() {
    }

    public Raspuns(String text, boolean valid) {
        this.text = text;
        this.valid = valid;
    }
}
