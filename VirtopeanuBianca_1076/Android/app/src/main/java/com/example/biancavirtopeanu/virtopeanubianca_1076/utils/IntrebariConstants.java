package com.example.biancavirtopeanu.virtopeanubianca_1076.utils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Bianca Virtopeanu on 12/23/2017.
 */

public interface IntrebariConstants {

    public String[] intrebariText = new String []{
            "Cum trebuie sa procedeze conducatorul unui autovehicul la trecerea la nivel cu calea ferata, atunci cand barierele sunt ridicate?",
            "Un conducator auto v-a depasit de mai multe ori, intrucat nu circula cu viteza constanta. Cum procedati atunci cand urmeaza sa fiti depasit din nou?"
            ,"Punctele de penalizare se aplica pentru:"
            ,"Conducatorul unui autovehicul angajat intr-un accident din care a rezultat moartea sau ranirea unei persoane poate parasi locul faptei fara incuviintarea politiei?"
            ,"Inspectia tehnica periodica a autovehiculelor si a remorcilor trebuie efectuata:"
            ,"Purtarea centurii de siguranta este obligatorie in timpul circulatiei pe drumurile din afara localitatilor?"
            , "Cand se retine certificatul de inmatriculare?"
            ,"Unde este interzisa oprirea vehiculelor?"
            ,"Ce obligatii va revin in ceea ce priveste regimul vitezei de circulatie?"
            ,"In ce situatie este interzisa intoarcerea autovehiculului?"
            ,"In ce situatii oprirea vehiculelor pe drumurile publice este interzisa?"
            ,"Depasirea nu este permisa:"
            ,"Vitezele maxime admise pentru subcategoria de autovehicule B1 sunt:"
            ,"Cum reactionati la aparitia unui obstacol pe un drum cu piatra cubica?"
            ,"Schimbarea directiei de mers spre stanga este interzisa atunci cand:"
            ,"Cauzele care determina consum crescut de combustibil sunt:"
            ,"Cand nu este permis unui autovehicul sa circule?"
            ,"Cum procedează conducătorul de autovehicule noaptea, când este orbit de lumina farurilor unui autovehicul care circulă din sens opus?"
            ,"În care dintre următoarele cazuri, pe lângă amenda contravenţională, se reţine şi permisul de conducere?"
            ,"În ce situaţie vă este permis să opriţi vehiculul sau să staţionaţi pe partea stângă a sensului de mers?"
            ,"Ce obligaţii are conducătorul unui autovehicul staţionat pe drumul public, dacă se îndepărtează de acesta?"
            ,"Care este rolul marcajelor rezonatoare?"
            ,"Vă apropiaţi de o trecere la nivel cu calea ferată curentă fără bariere, în traversarea căreia s-a angajat o căruţă aflată în faţa dvs. În această situaţie puteţi efectua depăşirea?"
            ,"Cum trebuie sa procedeze conducatorul unui autovehicul la trecerea la nivel cu calea ferata, atunci cand barierele sunt ridicate?",
            "Un conducator auto v-a depasit de mai multe ori, intrucat nu circula cu viteza constanta. Cum procedati atunci cand urmeaza sa fiti depasit din nou?"
            ,"Punctele de penalizare se aplica pentru:"
    };

    public int[] validAnswerIndex =new int[]{2,0,2,2,1,0,4,0,4,1,4,0,2,1,2,0,4,2,1,3,3,3,4,2,0,2};
}
