package com.example.biancavirtopeanu.virtopeanubianca_1076.network;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bianca Virtopeanu on 12/3/2017.
 */

public class CategorieInfo {
    private String tip;
    private int nrIntrebari;
    private int dificultate;
    private List<IntrebareInfo> intrebari;

    public CategorieInfo() {
    }

    public List<IntrebareInfo> getIntrebari() {
        return intrebari;
    }

    public void setIntrebari(List<IntrebareInfo> intrebari) {
        this.intrebari = intrebari;
    }

    public CategorieInfo(String tip, int nrIntrebari, int dificultate, List<IntrebareInfo> intrebari) {
        this.tip = tip;
        this.nrIntrebari = nrIntrebari;
        this.dificultate = dificultate;
        this.intrebari = intrebari;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public int getNrIntrebari() {
        return nrIntrebari;
    }

    public void setNrIntrebari(int nrIntrebari) {
        this.nrIntrebari = nrIntrebari;
    }

    public int getDificultate() {
        return dificultate;
    }

    public void setDificultate(int dificultate) {
        this.dificultate = dificultate;
    }

    @Override
    public String toString() {
        return "CategorieInfo{" +
                "tip='" + tip + '\'' +
                ", nrIntrebari=" + nrIntrebari +
                ", dificultate=" + dificultate +
                '}';
    }
}
