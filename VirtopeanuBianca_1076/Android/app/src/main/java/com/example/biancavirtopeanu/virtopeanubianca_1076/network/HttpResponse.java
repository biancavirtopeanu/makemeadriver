package com.example.biancavirtopeanu.virtopeanubianca_1076.network;

import java.util.List;

/**
 * Created by Bianca Virtopeanu on 12/3/2017.
 */

public class HttpResponse {
    private List<Item>categorieChestionar;

    public List<Item> getCategorieChestionar() {
        return categorieChestionar;
    }

    public void setCategorieChestionar(List<Item> categorieChestionar) {
        this.categorieChestionar = categorieChestionar;
    }

    public HttpResponse(List<Item> categorieChestionar) {
        this.categorieChestionar = categorieChestionar;
    }

    @Override
    public String toString() {
        return "HttpResponse{" +
                "categorieChestionar=" + categorieChestionar +
                '}';
    }
}
