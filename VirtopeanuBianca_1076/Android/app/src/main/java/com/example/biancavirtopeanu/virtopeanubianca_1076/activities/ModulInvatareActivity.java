package com.example.biancavirtopeanu.virtopeanubianca_1076.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;

import java.util.ArrayList;
import java.util.List;

import com.example.biancavirtopeanu.virtopeanubianca_1076.network.HttpConnection;
import com.example.biancavirtopeanu.virtopeanubianca_1076.network.HttpResponse;
import com.example.biancavirtopeanu.virtopeanubianca_1076.network.IntrebareInfo;
import com.example.biancavirtopeanu.virtopeanubianca_1076.network.Item;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.AbstractActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Intrebare;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Raspuns;

public class ModulInvatareActivity extends AbstractActivity {
    private CheckedTextView rasp1;
    private CheckedTextView rasp2;
    private CheckedTextView rasp3;
    private CheckedTextView rasp4;
    private CheckedTextView rasp5;
    private TextView intrebare;
    private int indexIntrebari=0;
    List<IntrebareInfo> intrebareList = new ArrayList<>();
    private ImageButton submit;
    private ImageButton next;
    private Item theItem;
    List<Item> items;
    Intrebare i1 = new Intrebare();
    Intrebare i2 = new Intrebare();
    private static String URL_JSON =
            "https://api.myjson.com/bins/1ctasb";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modul_invatare);
        setTitle(R.string.app_main_modul_invatare);

        rasp1=(CheckedTextView)findViewById(R.id.modInv_rasp1);
        rasp2=(CheckedTextView)findViewById(R.id.modInv_rasp2);
        rasp3=(CheckedTextView)findViewById(R.id.modInv_rasp3);
        rasp4=(CheckedTextView)findViewById(R.id.modInv_rasp4);
        rasp5=(CheckedTextView)findViewById(R.id.modInv_rasp5);
        intrebare=(TextView)findViewById(R.id.modInvatare_intrebare);
        submit=(ImageButton)findViewById(R.id.modInv_submit);
        next=(ImageButton)findViewById(R.id.modInv_chestionare_next);
        initalizeAnswers();
        @SuppressLint("StaticFieldLeak")
        final HttpConnection connection = new HttpConnection(){
            @Override
            protected void onPostExecute(HttpResponse httpResponse) {
                super.onPostExecute(httpResponse);
                if(httpResponse!=null){
                    items=httpResponse.getCategorieChestionar();
                    theItem =items.get(0);
                    intrebareList=theItem.getCategorie().getIntrebari();
                    i1.setText(theItem.getCategorie().getIntrebari().get(0).getText());
                    i1.setRasp1(theItem.getCategorie().getIntrebari().get(0).getRasp1());
                    i1.setRasp2(theItem.getCategorie().getIntrebari().get(0).getRasp2());
                    i1.setRasp3(theItem.getCategorie().getIntrebari().get(0).getRasp3());
                    i1.setRasp4(theItem.getCategorie().getIntrebari().get(0).getRasp4());
                    i1.setRasp5(theItem.getCategorie().getIntrebari().get(0).getRasp5());
                    intrebare.setText(i1.getText());
                    rasp1.setText(i1.getRasp1().getText());
                    rasp2.setText(i1.getRasp2().getText());
                    rasp3.setText(i1.getRasp3().getText());
                    rasp4.setText(i1.getRasp4().getText());
                    rasp5.setText(i1.getRasp5().getText());
                    indexIntrebari++;
                } else{
                    Toast.makeText(getApplicationContext(),
                            "Is empty", Toast.LENGTH_LONG).show();
                }
            }
        };
        connection.execute(URL_JSON);

        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(intrebareList.get(indexIntrebari).getRasp1().isValid()){
                    rasp1.setBackgroundColor(Color.GREEN);
                }
                if(intrebareList.get(indexIntrebari).getRasp2().isValid()){
                    rasp1.setBackgroundColor(Color.GREEN);
                }
                if(intrebareList.get(indexIntrebari).getRasp3().isValid()){
                    rasp1.setBackgroundColor(Color.GREEN);
                }
                if(intrebareList.get(indexIntrebari).getRasp4().isValid()){
                    rasp1.setBackgroundColor(Color.GREEN);
                }
                if(intrebareList.get(indexIntrebari).getRasp5().isValid()){
                    rasp1.setBackgroundColor(Color.GREEN);
                }

            }
        });

        next.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                rasp1.setBackgroundColor(0);
                rasp2.setBackgroundColor(0);
                rasp3.setBackgroundColor(0);
                rasp4.setBackgroundColor(0);
                rasp5.setBackgroundColor(0);
                if(indexIntrebari<intrebareList.size()){
                    i1.setText(theItem.getCategorie().getIntrebari().get(indexIntrebari).getText());
                    i1.setRasp1(theItem.getCategorie().getIntrebari().get(indexIntrebari).getRasp1());
                    i1.setRasp2(theItem.getCategorie().getIntrebari().get(indexIntrebari).getRasp2());
                    i1.setRasp3(theItem.getCategorie().getIntrebari().get(indexIntrebari).getRasp3());
                    i1.setRasp4(theItem.getCategorie().getIntrebari().get(indexIntrebari).getRasp4());
                    i1.setRasp5(theItem.getCategorie().getIntrebari().get(indexIntrebari).getRasp5());


                    intrebare.setText(i1.getText());
                    rasp1.setText(i1.getRasp1().getText());
                    rasp2.setText(i1.getRasp2().getText());
                    rasp3.setText(i1.getRasp3().getText());
                    rasp4.setText(i1.getRasp4().getText());
                    rasp5.setText(i1.getRasp5().getText());

                    indexIntrebari++;


                }else{
                    rasp1.setBackgroundColor(0);
                    rasp2.setBackgroundColor(0);
                    rasp3.setBackgroundColor(0);
                    rasp4.setBackgroundColor(0);
                    rasp5.setBackgroundColor(0);
                    indexIntrebari=0;
                    i1.setText(theItem.getCategorie().getIntrebari().get(0).getText());
                    i1.setRasp1(theItem.getCategorie().getIntrebari().get(0).getRasp1());
                    i1.setRasp2(theItem.getCategorie().getIntrebari().get(0).getRasp2());
                    i1.setRasp3(theItem.getCategorie().getIntrebari().get(0).getRasp3());
                    i1.setRasp4(theItem.getCategorie().getIntrebari().get(0).getRasp4());
                    i1.setRasp5(theItem.getCategorie().getIntrebari().get(0).getRasp5());
                    intrebare.setText(i1.getText());
                    rasp1.setText(i1.getRasp1().getText());
                    rasp2.setText(i1.getRasp2().getText());
                    rasp3.setText(i1.getRasp3().getText());
                    rasp4.setText(i1.getRasp4().getText());
                    rasp5.setText(i1.getRasp5().getText());
                    indexIntrebari++;
                }

            }
        });

    }


    void initalizeAnswers(){
        rasp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rasp1.isChecked()){
                    rasp1.setChecked(false);
                    rasp1.setBackgroundColor(0);
                }else{
                    rasp1.setChecked(true);
                    rasp1.setBackgroundColor(Color.YELLOW);
                }
            }});

        rasp2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rasp2.isChecked()){
                    rasp2.setChecked(false);
                    rasp2.setBackgroundColor(0);
                }else{
                    rasp2.setChecked(true);
                    rasp2.setBackgroundColor(Color.YELLOW);
                }
            }});

        rasp3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rasp3.isChecked()){
                    rasp3.setChecked(false);
                    rasp3.setBackgroundColor(0);
                }else{
                    rasp3.setChecked(true);
                    rasp3.setBackgroundColor(Color.YELLOW);
                }
            }});

        rasp4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rasp4.isChecked()){
                    rasp4.setChecked(false);
                    rasp4.setBackgroundColor(0);
                }else{
                    rasp4.setChecked(true);
                    rasp4.setBackgroundColor(Color.YELLOW);
                }
            }});

        rasp5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rasp5.isChecked()){
                    rasp5.setChecked(false);
                    rasp5.setBackgroundColor(0);
                }else{
                    rasp5.setChecked(true);
                    rasp5.setBackgroundColor(Color.YELLOW);
                }
            }});
    }

}
