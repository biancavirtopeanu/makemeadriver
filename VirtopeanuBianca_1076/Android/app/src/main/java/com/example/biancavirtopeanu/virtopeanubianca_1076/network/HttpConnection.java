package com.example.biancavirtopeanu.virtopeanubianca_1076.network;

import android.os.AsyncTask;

import com.example.biancavirtopeanu.virtopeanubianca_1076.activities.ChestionareActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletionService;

/**
 * Created by Bianca Virtopeanu on 12/3/2017.
 */

public class HttpConnection extends AsyncTask<String,Void,HttpResponse> {
    URL url;
    HttpURLConnection connection;
    private WeakReference<ChestionareActivity> activityReference;

    @Override
    protected HttpResponse doInBackground(String... params) {
        try {
            url = new URL(params[0]);//https://api.myjson.com/bins/1ctasb
            connection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = connection.getInputStream();
            InputStreamReader inputStreamReader =
                    new InputStreamReader(inputStream);
            BufferedReader reader =
                    new BufferedReader(inputStreamReader);

            String line = reader.readLine();

            reader.close();
            inputStreamReader.close();
            inputStream.close();
            connection.disconnect();

            JSONObject object = new JSONObject(line);
            Item categorii = getItemFromJson(object);
            List<Item> categoriiList =new ArrayList<>();
            categoriiList.add(categorii);
            HttpResponse response =new HttpResponse(categoriiList);
            return response;
            //getHttpResponseFromJson(line);

        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    private HttpResponse getHttpResponseFromJson(String json)
            throws JSONException {

        if (json == null) {
            return null;
        }

        JSONObject object = new JSONObject(json);

        List<Item> item =
                getArrayItemFromJson(object.getJSONArray(Constants.JSON_ITEM));

        return new HttpResponse(item);
    }

    private List<Item> getArrayItemFromJson(JSONArray array) throws JSONException {

        if (array == null) {
            return null;
        }

        List<Item> results = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            Item item = getItemFromJson(array.getJSONObject(i));
            if (item != null) {
                results.add(item);
            }
        }

        return results;

    }

    private Item getItemFromJson(JSONObject object) throws JSONException {

        if (object == null) {
            return null;
        }

        Item item = new Item();
        JSONArray itemJsonArray=object.getJSONArray(Constants.JSON_ITEM);
        JSONObject itemJsonObj = itemJsonArray.getJSONObject(0);
        item.setExtraInfo(itemJsonObj.getString(Constants.JSON_EXTRA_INFO));
        item.setNrChestionar(itemJsonObj.getInt(Constants.JSON_NR_CHESTIOANR));

        JSONObject categorieJson = itemJsonObj.getJSONObject(Constants.JSON_CATEGORIE);
        CategorieInfo categorie = new CategorieInfo();
        categorie.setDificultate(categorieJson.getInt(Constants.JSON_DIFICULTATE));
        categorie.setNrIntrebari(categorieJson.getInt(Constants.JSON_NR_INTREBARI));

        JSONArray intrebari= categorieJson.getJSONArray(Constants.JSON_INTREBARI);

        if (intrebari != null) {
            List<IntrebareInfo> intrebareInfoList=new ArrayList<IntrebareInfo>();
           for (int i=0;i<intrebari.length();i++){
               JSONObject intrebareJson=intrebari.getJSONObject(i);
               IntrebareInfo intrebare = new IntrebareInfo();
               intrebare.setText(intrebareJson.getString(Constants.JSON_TEXT_INTREBARE));
               JSONObject raspunsJson1=intrebareJson.getJSONObject(Constants.JSON_RASP1);
               JSONObject raspunsJson2=intrebareJson.getJSONObject(Constants.JSON_RASP2);
               JSONObject raspunsJson3=intrebareJson.getJSONObject(Constants.JSON_RASP3);
               JSONObject raspunsJson4=intrebareJson.getJSONObject(Constants.JSON_RASP4);
               JSONObject raspunsJson5=intrebareJson.getJSONObject(Constants.JSON_RASP5);
               RaspunsInfo r1=new RaspunsInfo();
               r1.setText(raspunsJson1.getString(Constants.JSON_TEXT_RASPUNS));
               r1.setValid(raspunsJson1.getBoolean(Constants.JSON_VALID));
               r1.setExtraInfo(raspunsJson1.getString(Constants.JSON_EXTRA_INFO_RASP));
               intrebare.setRasp1(r1);
               RaspunsInfo r2=new RaspunsInfo();
               r2.setText(raspunsJson2.getString(Constants.JSON_TEXT_RASPUNS));
               r2.setValid(raspunsJson2.getBoolean(Constants.JSON_VALID));
               r2.setExtraInfo(raspunsJson2.getString(Constants.JSON_EXTRA_INFO_RASP));
               intrebare.setRasp1(r2);
               RaspunsInfo r3=new RaspunsInfo();
               r3.setText(raspunsJson3.getString(Constants.JSON_TEXT_RASPUNS));
               r3.setValid(raspunsJson3.getBoolean(Constants.JSON_VALID));
               r3.setExtraInfo(raspunsJson3.getString(Constants.JSON_EXTRA_INFO_RASP));
               intrebare.setRasp1(r3);
               RaspunsInfo r4=new RaspunsInfo();
               r4.setText(raspunsJson4.getString(Constants.JSON_TEXT_RASPUNS));
               r4.setValid(raspunsJson4.getBoolean(Constants.JSON_VALID));
               r4.setExtraInfo(raspunsJson4.getString(Constants.JSON_EXTRA_INFO_RASP));
               intrebare.setRasp1(r4);
               RaspunsInfo r5=new RaspunsInfo();
               r5.setText(raspunsJson5.getString(Constants.JSON_TEXT_RASPUNS));
               r5.setValid(raspunsJson5.getBoolean(Constants.JSON_VALID));
               r5.setExtraInfo(raspunsJson5.getString(Constants.JSON_EXTRA_INFO_RASP));
               intrebare.setRasp1(r1);
               intrebare.setRasp2(r2);
               intrebare.setRasp3(r3);
               intrebare.setRasp4(r4);
               intrebare.setRasp5(r5);
               intrebareInfoList.add(intrebare);
           }

            categorie.setIntrebari(intrebareInfoList);
          item.setCategorie(categorie);
        }
        return item;
    }

}
