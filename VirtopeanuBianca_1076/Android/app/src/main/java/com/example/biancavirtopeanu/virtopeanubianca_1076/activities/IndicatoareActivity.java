package com.example.biancavirtopeanu.virtopeanubianca_1076.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;

import com.example.biancavirtopeanu.virtopeanubianca_1076.firebase.CloudStorageUploads;
import com.example.biancavirtopeanu.virtopeanubianca_1076.firebase.FirebaseController;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.AbstractActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Constants;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Indicator;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class IndicatoareActivity extends AbstractActivity  {
    private ImageView imageIndicatoare;
    private Button inainte;
    private Button inapoi;
    private ProgressBar progres;
    private TextView informatii;
    private CloudStorageUploads storage;
    private ArrayList<StorageReference> storageReferences;
    private int index=0;
    private FirebaseController firebaseController;
    private ArrayList<Indicator> indicators;
    ArrayList<String>urls=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indicatoare);
        setTitle(R.string.app_main_indicatoare);
        imageIndicatoare = (ImageView) findViewById(R.id.indicatoare_indicator);
        inainte=(Button)findViewById(R.id.tips_inainte);
        inapoi=(Button)findViewById(R.id.indicatoare_inapoi);
        progres=(ProgressBar)findViewById(R.id.indicatoare_progressBar);
        informatii=(TextView)findViewById(R.id.indicatoare_informatii);
        storage = new CloudStorageUploads();
        storage.createRef();
        storageReferences=storage.getIndicatoareRef();
        urls.add("https://firebasestorage.googleapis.com/v0/b/makemeadriver2.appspot.com/o/indicatoare%2Fi1.gif?alt=media&token=e7322936-bb03-42c2-9007-1fa02c2d8b73");
        urls.add("https://firebasestorage.googleapis.com/v0/b/makemeadriver2.appspot.com/o/indicatoare%2Fi2.gif?alt=media&token=f4c5589e-7f7b-4b7e-b142-6f8225b37cfa");
        urls.add("https://firebasestorage.googleapis.com/v0/b/makemeadriver2.appspot.com/o/indicatoare%2Fi3.gif?alt=media&token=25e8531c-1b51-4d1e-afaa-3cff51b3831e");
        urls.add("https://firebasestorage.googleapis.com/v0/b/makemeadriver2.appspot.com/o/indicatoare%2Fi4.gif?alt=media&token=ce670d51-e843-4bf8-81b0-e2c78f78f561");
        urls.add("https://firebasestorage.googleapis.com/v0/b/makemeadriver2.appspot.com/o/indicatoare%2Fi5.gif?alt=media&token=dd164713-89c2-48e6-b4d7-66208576228e");
        urls.add("https://firebasestorage.googleapis.com/v0/b/makemeadriver2.appspot.com/o/indicatoare%2Fi6.gif?alt=media&token=e864413b-c33c-410c-8cde-2ffdd91d29bd");
        urls.add("https://firebasestorage.googleapis.com/v0/b/makemeadriver2.appspot.com/o/indicatoare%2Fi7.gif?alt=media&token=a8743521-1bea-49df-9e77-1c32832500a9");
        urls.add("https://firebasestorage.googleapis.com/v0/b/makemeadriver2.appspot.com/o/indicatoare%2Fi8.gif?alt=media&token=ac140755-d2fc-4417-b76c-38936f78f72e");
        urls.add("https://firebasestorage.googleapis.com/v0/b/makemeadriver2.appspot.com/o/indicatoare%2Fi9.gif?alt=media&token=d7bdda7f-5c96-4b4e-a23e-43269173be2d");
//am incercat aici sa scriu in imageview direct cu referinta din storage, dar nu mi-a functionat, probabil motive de autentificare la storage//        for( StorageReference s: storageReferences){
//            s.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//                @Override
//                public void onSuccess(Uri uri) {
//                    urls.add(uri.toString());
//                }
//            }
//            ).addOnFailureListener(new OnFailureListener() {
//                @Override
//                public void onFailure(@NonNull Exception exception) {
//                    // Handle any errors
//                }
//            });
//        }

        //libraria Picasso am folosito pentru a incarca un link intr-un image view
        Picasso.with(getApplicationContext()).load(urls.get(index)).into(imageIndicatoare);
        firebaseController=FirebaseController.getInstance();
        firebaseController.findAllIndicator(uploadIndicatoareFromDatabaseGlobal());
    }

    private ValueEventListener uploadIndicatoareFromDatabaseGlobal() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                    indicators = new ArrayList<>();
                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Indicator indicator = data.getValue(Indicator.class);
                        if (indicator != null) {
                            indicators.add(indicator);
                            Log.i("IndicatoareActivity", "Selected Indicator: " + indicator.toString());
                        } else {
                            Log.i("IndicatoareActivity", "Selected Indicator is null");
                        }
                    }
                    //am scris codul asta aici pentru a evita asincronitatea
                progres.setProgress(0);
                    //aici nu e prea ok ca e hardcodat numarul 9, stiu, insa asta a fost prima solutia la care m-am gandit
                    //pentru a verifica daca au fost preluate toate datele
                if(indicators!=null && indicators.size()==9) {
                    informatii.setText(indicators.get(index).getIndicatorText());
                    inainte.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (index < urls.size()-1) {
                                index++;
                                informatii.setText(indicators.get(index).getIndicatorText());
                                Picasso.with(getApplicationContext()).load(urls.get(index)).into(imageIndicatoare);
                                progres.setProgress(index*10+10);
                            } else {
                                index = 0;
                                progres.setProgress(0);
                                informatii.setText(indicators.get(index).getIndicatorText());
                                Picasso.with(getApplicationContext()).load(urls.get(index)).into(imageIndicatoare);
                            }

                        }
                    });

                    inapoi.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (index > 0) {
                                index--;
                                informatii.setText(indicators.get(index).getIndicatorText());
                                Picasso.with(getApplicationContext()).load(urls.get(index)).into(imageIndicatoare);
                                progres.setProgress(index*10+10);
                            }
                        }
                    });
                }else{
                    //in cazul in care nu exista date in firebase, se introduc
                    //ca sa nu le introduc de 100 de ori pe aceleasi
                        if(indicators.size()==0){
                            insertDataFirebase();
                        }
                }
                }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("IndicatoareActivity", "Data is not available");
            }
        };
    }
    void insertDataFirebase(){
        firebaseController = FirebaseController.getInstance();
        Indicator i1 = new Indicator("i1Ref", Constants.i1Ref);
        i1.setId(new Long(1));
        Indicator i2 = new Indicator("i2Ref",Constants.i2Ref);
        i2.setId(new Long(2));
        Indicator i3 = new Indicator("i3Ref",Constants.i3Ref);
        i3.setId(new Long(3));
        Indicator i4 = new Indicator("i4Ref",Constants.i4Ref);
        i4.setId(new Long(4));
        Indicator i5 = new Indicator("i5Ref",Constants.i5Ref);
        i5.setId(new Long(5));
        Indicator i6 = new Indicator("i6Ref",Constants.i6Ref);
        i6.setId(new Long(6));
        Indicator i7 = new Indicator("i7Ref",Constants.i7Ref);
        i7.setId(new Long(7));
        Indicator i8 = new Indicator("i8Ref",Constants.i8Ref);
        i8.setId(new Long(8));
        Indicator i9 = new Indicator("i9Ref",Constants.i9Ref);
        i9.setId(new Long(9));
        ArrayList<Indicator> indicators = new ArrayList<>();
        indicators.add(i1);
        indicators.add(i2);
        indicators.add(i3);
        indicators.add(i4);
        indicators.add(i5);
        indicators.add(i6);
        indicators.add(i7);
        indicators.add(i8);
        indicators.add(i9);
        firebaseController.addAllIndicatoare(indicators);
    }
}
