package com.example.biancavirtopeanu.virtopeanubianca_1076.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;

import com.example.biancavirtopeanu.virtopeanubianca_1076.database.DatabaseRepository;
import com.example.biancavirtopeanu.virtopeanubianca_1076.firebase.FirebaseController;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.AbstractActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Constants;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Indicator;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Intrebare;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.IntrebariConstants;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Raspuns;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.RaspunsuriConstants;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class MainActivity extends AbstractActivity {

    private Button buttonLegislatie;
    private Button buttonChestionar;
    private Button buttonFeedback;
    private Button buttonIndicatoare;
    private Button buttonIstoric;
    private Button buttonModulInvatare;
    private Button buttonTips;
    private SharedPreferences preferences;
    private EditText nameText;
    private boolean insertedAlready=false;
    DatabaseRepository dbRepo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.app_main_home);
        buttonLegislatie=(Button)findViewById(R.id.home_legislatie_btn);
        buttonChestionar=(Button)findViewById(R.id.home_chestionare_btn);
        buttonFeedback=(Button) findViewById(R.id.home_feedback_btn);
        buttonIndicatoare=(Button)findViewById(R.id.home_indicatoare_btn);
        buttonIstoric=(Button)findViewById(R.id.home_istoric_btn);
        buttonModulInvatare=(Button)findViewById(R.id.home_modulInvatare_btn);
        buttonTips=(Button)findViewById(R.id.home_tips_btn);



        nameText=(EditText)findViewById(R.id.main_name);
        preferences = getSharedPreferences(Constants.
                MAIN_SHARED_PREFERENCES_NAME, MODE_PRIVATE);

        String nameFromFile = preferences.getString(Constants.
                MAIN_FEEDBACK_NAME, null);

        if (nameFromFile!=null) {
            nameText.setText(nameFromFile);
        }


        buttonLegislatie.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),LegislatieRutieraActivity.class);
                startActivity(intent);
            }
        });

        buttonChestionar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ChestionareActivity.class);
                startActivity(intent);
            }
        });
        buttonFeedback.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),FeedbackActivity.class);
                startActivity(intent);
            }
        });
        buttonTips.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),TipsAndTricksActivity.class);
                startActivity(intent);
            }
        });
        buttonModulInvatare.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ModulInvatareActivity.class);
                startActivity(intent);
            }
        });
        buttonIstoric.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),IstoricActivity.class);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(Constants.MAIN_FEEDBACK_NAME,nameText.getText().toString());
                //salvare fisier
                editor.commit();
                startActivity(intent);
            }
        });

        buttonIndicatoare.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),IndicatoareActivity.class);
                startActivity(intent);
            }
        });

        insertDataLocal();
    }


    private void insertDataLocal(){

        if(!insertedAlready){
            dbRepo=new DatabaseRepository(getApplicationContext());
            //pt a nu se insera aceleasi date de 1000 de ori
            dbRepo.deleteAndOpen();
            //momentan in raspunsuriConstants am pus doar un chestionar pentru test
            //se pot pune mai multe chestionare, adaugate mai apoi intr-o lista din care sa se aleaga random un chestionar
            //apoi se poate aplica mecanismul de mai jos
            //voi incerca sa implementez pana la prezentare
            String[] raspunsuriText=RaspunsuriConstants.raspunsuriTexts;
            String[] intrebariText= IntrebariConstants.intrebariText;
            int [] validAnswers=IntrebariConstants.validAnswerIndex;
            int validIndex=0;
            int i=0;
            for(String intrebareText:intrebariText){
                Long id1,id2,id3,id4,id5;
                Intrebare intrebare = new Intrebare();
                intrebare.setText(intrebareText);
                int index=0;
                while (index<5 && i<raspunsuriText.length){
                    Raspuns raspuns = new Raspuns();
                    raspuns.setText(raspunsuriText[i]);
                    if(index==validAnswers[validIndex]){
                        raspuns.setValid(true);
                    }
                    if(index==0){
                        intrebare.setRasp1(raspuns);
                        id1=dbRepo.insertRaspuns(intrebare.getRasp1());
                        intrebare.getRasp1().setId(id1);
                    }
                    if(index==1){
                        intrebare.setRasp2(raspuns);
                        id2=dbRepo.insertRaspuns(intrebare.getRasp2());
                        intrebare.getRasp2().setId(id2);
                    }
                    if(index==2){
                        intrebare.setRasp3(raspuns);
                        id3=dbRepo.insertRaspuns(intrebare.getRasp3());
                        intrebare.getRasp3().setId(id3);
                    }
                    if(index==3){
                        intrebare.setRasp4(raspuns);
                        id4=dbRepo.insertRaspuns(intrebare.getRasp4());
                        intrebare.getRasp4().setId(id4);
                    }
                    if(index==4){
                        intrebare.setRasp5(raspuns);
                        id5=dbRepo.insertRaspuns(intrebare.getRasp5());
                        intrebare.getRasp5().setId(id5);

                    }
                    if(index==4){
                        dbRepo.insertIntrebare(intrebare);
                    }
                    if(index<5){
                        index++;

                    }else{
                        index=0;
                    }
                        i++;
                }
                validIndex++;

            }
            dbRepo.close();
        }

    }

}
