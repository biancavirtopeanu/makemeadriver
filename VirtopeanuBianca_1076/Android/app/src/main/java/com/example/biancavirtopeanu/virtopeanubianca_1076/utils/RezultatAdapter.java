package com.example.biancavirtopeanu.virtopeanubianca_1076.utils;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;

import java.util.List;

/**
 * Created by Bianca Virtopeanu on 11/18/2017.
 */

public class RezultatAdapter extends ArrayAdapter {
    private int resource;
    private List<Rezultat> objects;
    private LayoutInflater inflater;

    public RezultatAdapter(@NonNull Context context,
                           @LayoutRes int resource,
                           @NonNull List objects,
                           LayoutInflater inflater) {
        super(context, resource, objects);

        this.resource = resource;
        this.objects = objects;
        this.inflater = inflater;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView,
                        @NonNull ViewGroup parent) {

        View row = inflater.inflate(this.resource, parent, false);

        TextView punctaj = (TextView) row.findViewById(R.id.lv_istoric_result);
        TextView date = (TextView) row.findViewById(R.id.istoric_date);
        TextView timp =(TextView)row.findViewById(R.id.lv_istoric_timp);

        Rezultat rezultat = objects.get(position);

        punctaj.setText(rezultat != null ? "Punctajul:" + Integer.toString(rezultat.getPunctaj()) : "NO DATE");
        date.setText(date != null && rezultat.getData() != null ?
                Constants.simpleDateFormat.format(rezultat.getData()) : "NO DATE");
        timp.setText(rezultat !=null ? "Timp:" + Integer.toString(rezultat.getTimp()): "NO TIME");

        return row;

    }

}
