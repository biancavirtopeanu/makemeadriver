package com.example.biancavirtopeanu.virtopeanubianca_1076.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;

import java.util.ArrayList;
import java.util.List;

import com.example.biancavirtopeanu.virtopeanubianca_1076.database.DatabaseRepository;
import com.example.biancavirtopeanu.virtopeanubianca_1076.network.HttpConnection;
import com.example.biancavirtopeanu.virtopeanubianca_1076.network.HttpResponse;
import com.example.biancavirtopeanu.virtopeanubianca_1076.network.IntrebareInfo;
import com.example.biancavirtopeanu.virtopeanubianca_1076.network.Item;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.AbstractActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Constants;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Intrebare;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Raspuns;

public class ChestionareActivity extends AbstractActivity implements Constants {

    private CheckedTextView rasp1;
    private CheckedTextView rasp2;
    private CheckedTextView rasp3;
    private CheckedTextView rasp4;
    private CheckedTextView rasp5;
    private TextView seconds;
    private TextView minutes;
    private TextView separator;
    private int counterSecunde;
    private int counterMinute;
    private TextView intrebareTv;
    private int indexIntrebari=0;
    private ImageButton submit;
    private ImageButton next;
    private int rezultat=0;
    private List<Intrebare> intrebari;
    private Intrebare intrebare;
    ArrayList<Integer> data =new ArrayList<>();
    private DatabaseRepository dbRepo;
    private Button finalizare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chestionare);
        setTitle(R.string.app_main_chestionare);
        rasp1=(CheckedTextView)findViewById(R.id.rasp1);
        rasp2=(CheckedTextView)findViewById(R.id.rasp2);
        rasp3=(CheckedTextView)findViewById(R.id.rasp3);
        rasp4=(CheckedTextView)findViewById(R.id.rasp4);
        rasp5=(CheckedTextView)findViewById(R.id.rasp5);
        intrebareTv=(TextView)findViewById(R.id.intrebare);
        submit=(ImageButton)findViewById(R.id.chestionare_submit);
        next=(ImageButton)findViewById(R.id.chestionare_next);
        seconds =(TextView)findViewById(R.id.chestionare_secunde);
        minutes=(TextView)findViewById(R.id.chestionare_minute);
        separator=(TextView)findViewById(R.id.chestionare_separator);
        finalizare=(Button)findViewById(R.id.chestionare_finalizarebtn);

        intrebare=new Intrebare();
        this.setClickableAnswers();
        this.setCountdown();
        this.setButtonsNextAndSubmit();
        getIntrebari();
        finalizeaza();

    }

    private  void finalizeaza(){
        finalizare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),RezultatChestionarActivity.class);
                intent.putExtra(punctaj,rezultat);
                intent.putExtra(timp,counterMinute);
                setPyramidData();
                intent.putIntegerArrayListExtra(Constants.PYRAMID_DATA,data);
                startActivity(intent);
            }
        });
    }

    private void getIntrebari(){
        dbRepo=new DatabaseRepository(getApplicationContext());
        dbRepo.open();
        intrebari=dbRepo.findAllIntrebari();
        if(intrebari!=null) {
            if (indexIntrebari < intrebari.size()) {
                intrebare.setText(intrebari.get(indexIntrebari).getText());
                intrebare.setRasp1(intrebari.get(indexIntrebari).getRasp1());
                intrebare.setRasp2(intrebari.get(indexIntrebari).getRasp2());
                intrebare.setRasp3(intrebari.get(indexIntrebari).getRasp3());
                intrebare.setRasp4(intrebari.get(indexIntrebari).getRasp4());
                intrebare.setRasp5(intrebari.get(indexIntrebari).getRasp5());
                intrebareTv.setText(intrebare.getText());
                rasp1.setText(intrebare.getRasp1().getText());
                rasp2.setText(intrebare.getRasp2().getText());
                rasp3.setText(intrebare.getRasp3().getText());
                rasp4.setText(intrebare.getRasp4().getText());
                rasp5.setText(intrebare.getRasp5().getText());
                indexIntrebari++;
            }
        }
        dbRepo.close();
    }

    private void setClickableAnswers(){
        rasp1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rasp1.isChecked()){
                    rasp1.setChecked(false);
                    if(intrebare.getRasp1().isValid()){
                        rezultat--;
                    }
                    rasp1.setBackgroundColor(0);
                }else{
                    rasp1.setChecked(true);
                    if(intrebare.getRasp1().isValid()){
                        rezultat++;
                    }
                    rasp1.setBackgroundColor(Color.parseColor("#0000CD"));
                }
            }});

        rasp2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rasp2.isChecked()){
                    rasp2.setChecked(false);
                    if(intrebare.getRasp2().isValid()){
                        rezultat--;
                    }
                    rasp2.setBackgroundColor(0);
                }else{
                    rasp2.setChecked(true);
                    if(intrebare.getRasp2().isValid()){
                        rezultat++;
                    }
                    rasp2.setBackgroundColor(Color.parseColor("#0000CD"));
                }
            }});

        rasp3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rasp3.isChecked()){
                    rasp3.setChecked(false);
                    if(intrebare.getRasp3().isValid()){
                        rezultat--;
                    }
                    rasp3.setBackgroundColor(0);
                }else{
                    rasp3.setChecked(true);
                    if(intrebare.getRasp3().isValid()){
                        rezultat++;
                    }
                    rasp3.setBackgroundColor(Color.parseColor("#0000CD"));
                }
            }});

        rasp4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rasp4.isChecked()){
                    rasp4.setChecked(false);
                    if(intrebare.getRasp4().isValid()){
                        rezultat++;
                    }
                    rasp4.setBackgroundColor(0);
                }else{
                    rasp4.setChecked(true);
                    if(intrebare.getRasp4().isValid()){
                        rezultat++;
                    }
                    rasp4.setBackgroundColor(Color.parseColor("#0000CD"));
                }
            }});

        rasp5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(rasp5.isChecked()){
                    rasp5.setChecked(false);
                    if(intrebare.getRasp5().isValid()){
                        rezultat--;
                    }
                    rasp5.setBackgroundColor(0);
                }else{
                    rasp5.setChecked(true);
                    if(intrebare.getRasp5().isValid()){
                        rezultat++;
                    }
                    rasp5.setBackgroundColor(Color.parseColor("#0000CD"));
                }
            }});
    }

    private void setCountdown(){
        //COUNTDOWN
        //o sa fie 1800000 milisec
        new CountDownTimer(1800000,1000){

            @Override
            public void onTick(long millisUntilFinished) {
                seconds.setText(String.valueOf(counterSecunde));
                counterSecunde++;
                if(counterSecunde==60){
                    counterMinute++;
                    minutes.setText(String.valueOf(counterMinute));
                    counterSecunde=0;
                }
            }
            @Override
            public void onFinish() {
                seconds.setText(R.string.chestionare_timp_scurs);
                minutes.setText("");
                separator.setText("");
                Intent intent = new Intent(getApplicationContext(),RezultatChestionarActivity.class);
                intent.putExtra(punctaj,rezultat);
                intent.putExtra(timp,counterMinute);
                setPyramidData();
                intent.putIntegerArrayListExtra(Constants.PYRAMID_DATA,data);
                startActivity(intent);
            }
        }.start();
    }

    private void setButtonsNextAndSubmit(){
        submit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(intrebari!=null) {
                    if (indexIntrebari < intrebari.size()) {
                        intrebare.setText(intrebari.get(indexIntrebari).getText());
                        intrebare.setRasp1(intrebari.get(indexIntrebari).getRasp1());
                        intrebare.setRasp2(intrebari.get(indexIntrebari).getRasp2());
                        intrebare.setRasp3(intrebari.get(indexIntrebari).getRasp3());
                        intrebare.setRasp4(intrebari.get(indexIntrebari).getRasp4());
                        intrebare.setRasp5(intrebari.get(indexIntrebari).getRasp5());
                        intrebareTv.setText(intrebare.getText());
                        rasp1.setText(intrebare.getRasp1().getText());
                        rasp2.setText(intrebare.getRasp2().getText());
                        rasp3.setText(intrebare.getRasp3().getText());
                        rasp4.setText(intrebare.getRasp4().getText());
                        rasp5.setText(intrebare.getRasp5().getText());
                        indexIntrebari++;
                        //verificam raspunsul

                        if(rasp1.isChecked() && intrebari.get(indexIntrebari).getRasp1().isValid()){
                            rezultat++;
                        }
                        if(rasp2.isChecked() && intrebari.get(indexIntrebari).getRasp2().isValid()){
                            rezultat++;
                        }
                        if(rasp3.isChecked() && intrebari.get(indexIntrebari).getRasp3().isValid()){
                            rezultat++;
                        }
                        if(rasp4.isChecked() && intrebari.get(indexIntrebari).getRasp4().isValid()){
                            rezultat++;
                        }
                        if(rasp5.isChecked() && intrebari.get(indexIntrebari).getRasp5().isValid()){
                            rezultat++;
                        }

                    } else {
                        indexIntrebari = 0;
                        intrebare.setText(intrebari.get(indexIntrebari).getText());
                        intrebare.setRasp1(intrebari.get(indexIntrebari).getRasp1());
                        intrebare.setRasp2(intrebari.get(indexIntrebari).getRasp2());
                        intrebare.setRasp3(intrebari.get(indexIntrebari).getRasp3());
                        intrebare.setRasp4(intrebari.get(indexIntrebari).getRasp4());
                        intrebare.setRasp5(intrebari.get(indexIntrebari).getRasp5());
                        intrebareTv.setText(intrebare.getText());
                        rasp1.setText(intrebare.getRasp1().getText());
                        rasp2.setText(intrebare.getRasp2().getText());
                        rasp3.setText(intrebare.getRasp3().getText());
                        rasp4.setText(intrebare.getRasp4().getText());
                        rasp5.setText(intrebare.getRasp5().getText());
                        indexIntrebari++;

                        //verificam raspuns
                        if(rasp1.isChecked() && intrebari.get(indexIntrebari).getRasp1().isValid()){
                            rezultat++;
                        }
                        if(rasp2.isChecked() && intrebari.get(indexIntrebari).getRasp2().isValid()){
                            rezultat++;
                        }
                        if(rasp3.isChecked() && intrebari.get(indexIntrebari).getRasp3().isValid()){
                            rezultat++;
                        }
                        if(rasp4.isChecked() && intrebari.get(indexIntrebari).getRasp4().isValid()){
                            rezultat++;
                        }
                        if(rasp5.isChecked() && intrebari.get(indexIntrebari).getRasp5().isValid()){
                            rezultat++;
                        }
                    }
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(intrebari!=null) {
                    if (indexIntrebari < intrebari.size()) {
                        intrebare.setText(intrebari.get(indexIntrebari).getText());
                        intrebare.setRasp1(intrebari.get(indexIntrebari).getRasp1());
                        intrebare.setRasp2(intrebari.get(indexIntrebari).getRasp2());
                        intrebare.setRasp3(intrebari.get(indexIntrebari).getRasp3());
                        intrebare.setRasp4(intrebari.get(indexIntrebari).getRasp4());
                        intrebare.setRasp5(intrebari.get(indexIntrebari).getRasp5());
                        intrebareTv.setText(intrebare.getText());
                        rasp1.setText(intrebare.getRasp1().getText());
                        rasp2.setText(intrebare.getRasp2().getText());
                        rasp3.setText(intrebare.getRasp3().getText());
                        rasp4.setText(intrebare.getRasp4().getText());
                        rasp5.setText(intrebare.getRasp5().getText());
                        indexIntrebari++;
                        //verificam raspunsuri
                        if(rasp1.isChecked() && intrebari.get(indexIntrebari).getRasp1().isValid()){
                            rezultat++;
                        }
                        if(rasp2.isChecked() && intrebari.get(indexIntrebari).getRasp2().isValid()){
                            rezultat++;
                        }
                        if(rasp3.isChecked() && intrebari.get(indexIntrebari).getRasp3().isValid()){
                            rezultat++;
                        }
                        if(rasp4.isChecked() && intrebari.get(indexIntrebari).getRasp4().isValid()){
                            rezultat++;
                        }
                        if(rasp5.isChecked() && intrebari.get(indexIntrebari).getRasp5().isValid()){
                            rezultat++;
                        }
                    } else {
                        indexIntrebari = 0;
                        intrebare.setText(intrebari.get(indexIntrebari).getText());
                        intrebare.setRasp1(intrebari.get(indexIntrebari).getRasp1());
                        intrebare.setRasp2(intrebari.get(indexIntrebari).getRasp2());
                        intrebare.setRasp3(intrebari.get(indexIntrebari).getRasp3());
                        intrebare.setRasp4(intrebari.get(indexIntrebari).getRasp4());
                        intrebare.setRasp5(intrebari.get(indexIntrebari).getRasp5());
                        intrebareTv.setText(intrebare.getText());
                        rasp1.setText(intrebare.getRasp1().getText());
                        rasp2.setText(intrebare.getRasp2().getText());
                        rasp3.setText(intrebare.getRasp3().getText());
                        rasp4.setText(intrebare.getRasp4().getText());
                        rasp5.setText(intrebare.getRasp5().getText());
                        indexIntrebari++;
                        //verificam raspuns
                        if(rasp1.isChecked() && intrebari.get(indexIntrebari).getRasp1().isValid()){
                            rezultat++;
                        }
                        if(rasp2.isChecked() && intrebari.get(indexIntrebari).getRasp2().isValid()){
                            rezultat++;
                        }
                        if(rasp3.isChecked() && intrebari.get(indexIntrebari).getRasp3().isValid()){
                            rezultat++;
                        }
                        if(rasp4.isChecked() && intrebari.get(indexIntrebari).getRasp4().isValid()){
                            rezultat++;
                        }
                        if(rasp5.isChecked() && intrebari.get(indexIntrebari).getRasp5().isValid()){
                            rezultat++;
                        }
                    }
                }
            }
        });

    }

    void setPyramidData(){
        //calculez numarul de raspunsuri valide de fiecare tip
        //cate de rasp1 sunt corecte din toate spre exemplu
        int nrRasp1Valid=0;
        int nrRasp2Valid=0;
        int nrRasp3Valid=0;
        int nrRasp4Valid=0;
        int nrRasp5Valid=0;
        if(intrebari!=null){
            for(Intrebare i:intrebari){
                if(i.getRasp1().isValid()){
                    nrRasp1Valid++;
                }
                if(i.getRasp2().isValid()){
                    nrRasp2Valid++;
                }
                if(i.getRasp3().isValid()){
                    nrRasp3Valid++;
                }
                if(i.getRasp4().isValid()){
                    nrRasp4Valid++;
                }
                if(i.getRasp5().isValid()){
                    nrRasp5Valid++;
                }
            }
        }

        data.add(nrRasp1Valid);data.add(nrRasp2Valid);data.add(nrRasp3Valid);data.add(nrRasp4Valid);data.add(nrRasp5Valid);
    }


}
