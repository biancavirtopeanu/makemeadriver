package com.example.biancavirtopeanu.virtopeanubianca_1076.network;

import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Intrebare;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Raspuns;

/**
 * Created by Bianca Virtopeanu on 12/3/2017.
 */

public class IntrebareInfo extends Intrebare {
    String text;
    RaspunsInfo rasp1;
    RaspunsInfo rasp2;
    RaspunsInfo rasp3;
    RaspunsInfo rasp4;
    RaspunsInfo rasp5;

    public IntrebareInfo(Long id, String text, Raspuns rasp1, Raspuns rasp2, Raspuns rasp3, Raspuns rasp4, Raspuns rasp5) {
        super(id, text, rasp1, rasp2, rasp3, rasp4, rasp5);
    }

    public IntrebareInfo(String text, Raspuns rasp1, Raspuns rasp2, Raspuns rasp3, Raspuns rasp4, Raspuns rasp5) {
        super(text, rasp1, rasp2, rasp3, rasp4, rasp5);
    }

    public IntrebareInfo() {
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public RaspunsInfo getRasp1() {
        return rasp1;
    }

    public void setRasp1(RaspunsInfo rasp1) {
        this.rasp1 = rasp1;
    }

    @Override
    public RaspunsInfo getRasp2() {
        return rasp2;
    }

    public void setRasp2(RaspunsInfo rasp2) {
        this.rasp2 = rasp2;
    }

    @Override
    public RaspunsInfo getRasp3() {
        return rasp3;
    }

    public void setRasp3(RaspunsInfo rasp3) {
        this.rasp3 = rasp3;
    }

    @Override
    public RaspunsInfo getRasp4() {
        return rasp4;
    }

    public void setRasp4(RaspunsInfo rasp4) {
        this.rasp4 = rasp4;
    }

    @Override
    public RaspunsInfo getRasp5() {
        return rasp5;
    }

    public void setRasp5(RaspunsInfo rasp5) {
        this.rasp5 = rasp5;
    }

    public IntrebareInfo(Long id, String text, Raspuns rasp1, Raspuns rasp2, Raspuns rasp3, Raspuns rasp4, Raspuns rasp5, String text1, RaspunsInfo rasp11, RaspunsInfo rasp21, RaspunsInfo rasp31, RaspunsInfo rasp41, RaspunsInfo rasp51) {
        super(id, text, rasp1, rasp2, rasp3, rasp4, rasp5);
        this.text = text1;
        this.rasp1 = rasp11;
        this.rasp2 = rasp21;
        this.rasp3 = rasp31;
        this.rasp4 = rasp41;
        this.rasp5 = rasp51;
    }

    public IntrebareInfo(String text, Raspuns rasp1, Raspuns rasp2, Raspuns rasp3, Raspuns rasp4, Raspuns rasp5, String text1, RaspunsInfo rasp11, RaspunsInfo rasp21, RaspunsInfo rasp31, RaspunsInfo rasp41, RaspunsInfo rasp51) {
        super(text, rasp1, rasp2, rasp3, rasp4, rasp5);
        this.text = text1;
        this.rasp1 = rasp11;
        this.rasp2 = rasp21;
        this.rasp3 = rasp31;
        this.rasp4 = rasp41;
        this.rasp5 = rasp51;
    }
}
