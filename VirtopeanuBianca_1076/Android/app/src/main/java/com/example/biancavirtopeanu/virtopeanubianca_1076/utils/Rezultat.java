package com.example.biancavirtopeanu.virtopeanubianca_1076.utils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Bianca Virtopeanu on 11/18/2017.
 */

public class Rezultat implements Serializable {
    private Date data;
    private int punctaj;
    private int timp;

    public Rezultat() {
    }

    public Rezultat(Date data, int punctaj,int timp) {
        this.data = data;
        this.punctaj = punctaj;
        this.timp=timp;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getPunctaj() {
        return punctaj;
    }

    public void setPunctaj(int punctaj) {
        this.punctaj = punctaj;
    }

    public int getTimp() {
        return timp;
    }

    public void setTimp(int timp) {
        this.timp = timp;
    }

    @Override
    public String toString() {
        return "Rezultat{" +
                "data=" + data +
                ", punctaj=" + punctaj +
                ", timp=" + timp +
                '}';
    }


}
