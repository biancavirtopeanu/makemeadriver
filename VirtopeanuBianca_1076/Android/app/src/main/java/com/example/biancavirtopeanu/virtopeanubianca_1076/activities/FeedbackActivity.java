package com.example.biancavirtopeanu.virtopeanubianca_1076.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.Toast;

import com.example.biancavirtopeanu.virtopeanubianca_1076.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.example.biancavirtopeanu.virtopeanubianca_1076.database.DatabaseRepository;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.AbstractActivity;
import com.example.biancavirtopeanu.virtopeanubianca_1076.utils.Feedback;

public class FeedbackActivity extends AbstractActivity {

    private Button submitFeedback;
    private CheckedTextView checkMail;
    private EditText nume;
    private EditText prenume;
    private EditText email;
    private EditText parere;
    private DatabaseRepository dbrepo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        setTitle(R.string.feedback);
        submitFeedback=(Button)findViewById(R.id.button);
        checkMail=(CheckedTextView) findViewById(R.id.feedbackCheckMail);
        nume=(EditText)findViewById(R.id.feedbackNume);
        prenume=(EditText)findViewById(R.id.feedbackPrenume);
        email=(EditText)findViewById(R.id.feedbackEmail);
        parere=(EditText)findViewById(R.id.feedbackParere);

        submitFeedback.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(validateFields()==true){
                    dbrepo=new DatabaseRepository(getApplicationContext());
                    dbrepo.open();
                    Feedback feedback=new Feedback(nume.getText().toString(),prenume.getText().toString(),email.getText().toString(),parere.getText().toString());
                    feedback.setEmailChecked(checkMail.isChecked());
                    dbrepo.insertFeedback(feedback);
                    List<Feedback> allFeedback=dbrepo.getAllFeedback();
                    writeToFile(allFeedback,getApplicationContext());
                    dbrepo.close();
                    Toast.makeText(getApplicationContext(), R.string.feedback_mesaj,
                            Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                }

            }
        });

        checkMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkMail.isChecked()){
                    checkMail.setChecked(false);
                }else{
                    checkMail.setChecked(true);
                }
            }});
    }

    boolean validateFields(){

        Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
        Matcher mat = pattern.matcher(email.getText().toString().trim());

        if(nume.getText().length()<=0 || prenume.getText().length()<=0 || email.getText().length()<=0 || parere.getText().length()<=0){
            Toast.makeText(getApplicationContext(), R.string.feedback_mesajavertizare,
                    Toast.LENGTH_SHORT).show();
                    return false;
        }

        if(!mat.matches()) {
            Toast.makeText(getApplicationContext(), R.string.feedback_message,
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void writeToFile(List<Feedback> data, Context context) {
        //open this with notepad ++
        File file=context.getFilesDir();
        String path = file.getAbsolutePath();
        String filename = "feedback.txt";
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(filename, Context.MODE_APPEND);
            for (Feedback f : data) {
                outputStream.write(f.getNume().getBytes());
                outputStream.write("\n".getBytes());
                outputStream.write(f.getPrenume().getBytes());
                outputStream.write("\n".getBytes());
                outputStream.write(f.getEmail().getBytes());
                outputStream.write("\n".getBytes());
                outputStream.write(f.getFeedbackText().getBytes());
                outputStream.write("****************".getBytes());
                outputStream.write("\n".getBytes());
                outputStream.write("\n".getBytes());

            }
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
